using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

namespace FlexiUp.Processors {
    class CopyFileProcessor : FileProcessorDecorator {
        private string targetPath;
        private PathBuilder pathBuilder = new PathBuilder();

        public CopyFileProcessor(string _targetPath, IFileProcessor _parent) : base(_parent){
            this.targetPath = _targetPath;
        }

        protected string getTargetPath(string srcFullPath) {
            return pathBuilder.GetExpandedTarget(srcFullPath, targetPath);
        }

        #region IFileProcessor Members

        public override void ProcessFile(string fullPath) {
            string targetPath = this.getTargetPath(fullPath);
            Directory.CreateDirectory(Path.GetDirectoryName(targetPath));
            File.Copy(fullPath, targetPath, true);

            base.ProcessFile(fullPath);
        }

        #endregion
    }
}
