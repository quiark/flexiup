using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FlexiUp.Processors {
    public class LogMaker : FileProcessorDecorator {
        protected TextWriter writer;
        public const string Separator = " /?/ ";

        public LogMaker(IFileProcessor _parent, TextWriter _writer)
            : base(_parent) {
            this.writer = _writer;
        }

        public override void ProcessFile(string fullPath) {

            FileInfo info = new FileInfo(fullPath);
            string line = String.Format("{1}{0}{2}{0}{3}", Separator, fullPath, info.Length, info.LastWriteTime.ToString());
            writer.WriteLine(line);

            base.ProcessFile(fullPath);

        }

        public override void Finish() {
            base.Finish();

            writer.Close();
            writer.Dispose();
        }
    }
}
