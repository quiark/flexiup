using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Processors {
    /// <summary>
    /// As every IncrementalFilter only works with one log file, this class helps manage a bunch of these filters
    /// when we're working with several log files.
    /// </summary>
    class IncFilterGroup {

        private List<IncrementalFilter> filters = new List<IncrementalFilter>();

        public IncFilterGroup(IList<string> logFiles) {
            foreach (string incLog in logFiles) {
                filters.Add(new IncrementalFilter(null, incLog));
            }
        }

        public bool IsModified(string fullPath) {
            foreach (IncrementalFilter filter in filters) {
                if (!filter.Passed(fullPath))
                    return false;
            }

            return true;
        }
    }
}
