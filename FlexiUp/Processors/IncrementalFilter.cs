using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using naid;

namespace FlexiUp.Processors {

    public class IncrementalFilter {

        protected class LogEntry {
            public string FileName;
            public int Size;
            public DateTime ModificationTime;
        }

        // List of entries in the log sorted by FileName.
        protected IList<LogEntry> log;

        public IncrementalFilter(IFileProcessor _parent, string logFile) {
            this.log = this.parseLog(logFile);
        }

        protected IList<LogEntry> parseLog(string logFile) {
            List<LogEntry> res = new List<LogEntry>();

            using (TextReader reader = new StreamReader(logFile)) {
                string line;
                while ((line = reader.ReadLine()) != null) {
                    string[] parts = line.Split(new string[] { LogMaker.Separator }, StringSplitOptions.None);
                    
                    LogEntry entry = new LogEntry();
                    entry.FileName = parts[0].ToLowerInvariant();
                    entry.Size = int.Parse(parts[1]);
                    entry.ModificationTime = DateTime.Parse(parts[2]);

                    res.Add(entry);
                    
                }
            }

            res.Sort(delegate(LogEntry a, LogEntry b) {
                return String.CompareOrdinal(a.FileName, b.FileName);
            });

            return res;
        }

        public bool Passed(string fullPath) {
            FileInfo file = new FileInfo(fullPath);
            LogEntry entry = this.findLogEntry(fullPath);
            if (entry == null)
                return true;

            bool datesSame = (entry.ModificationTime.Year == file.LastWriteTime.Year) &&
                (entry.ModificationTime.Month == file.LastWriteTime.Month) &&
                (entry.ModificationTime.Day == file.LastWriteTime.Day) &&
                (entry.ModificationTime.Hour == file.LastWriteTime.Hour) &&
                (entry.ModificationTime.Minute == file.LastWriteTime.Minute) &&
                (entry.ModificationTime.Second == file.LastWriteTime.Second);

            return (entry.Size != file.Length) || (!datesSame);
        }

        protected LogEntry findLogEntry(string fullPath) {
            string fullPath_lower = fullPath.ToLowerInvariant();
            
            return ListAlgorithms<LogEntry>.BinarySearch(this.log, delegate(LogEntry item) {
                return String.CompareOrdinal(item.FileName, fullPath_lower);
            });
        }
    }
}
