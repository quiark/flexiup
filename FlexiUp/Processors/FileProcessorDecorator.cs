namespace FlexiUp.Processors
{
    public class FileProcessorDecorator : IFileProcessor{
        protected IFileProcessor parent;

        public FileProcessorDecorator(IFileProcessor _parent) {
            this.parent = _parent;
        }

        

        #region IFileProcessor Members

        public virtual void ProcessFile(string fullPath) {
            if (this.parent != null) {
                this.parent.ProcessFile(fullPath);
            }
        }

        public virtual void Finish() {
            if (this.parent != null)
                this.parent.Finish();
        }

        #endregion
    }
}
