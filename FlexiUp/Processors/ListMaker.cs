using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Processors {
    public class ListMaker : FileProcessorDecorator {
        private List<string> filesList;
        public ICollection<string> FilesList {
            get {
                return this.filesList;
            }
        }

        public ListMaker(IFileProcessor _parent) : base(_parent){
            this.filesList = new List<string>();
        }

        #region IFileProcessor Members

        public override void ProcessFile(string fullPath) {
            base.ProcessFile(fullPath);

            string mod = fullPath.ToLower();

            if (!this.filesList.Contains(mod))
                this.filesList.Add(mod);
        }

        #endregion
    }
}
