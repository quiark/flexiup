using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Processors {
    /// <summary>
    /// This class makes a list of files. The files in the list are then sent
    /// to the parent processor. This way all duplicates are removed.
    /// </summary>
    public class ListFilter : FileProcessorDecorator {

        private class DictToEnum: IEnumerable<string>, IEnumerator<string> {
            private IDictionary<string, string> src;
            private IEnumerator< KeyValuePair<string, string> > srcEnum;

            public DictToEnum(IDictionary<string, string> _src) {
                src = _src;
                srcEnum = src.GetEnumerator();
            }

            #region IEnumerator<string> Members
            string IEnumerator<string>.Current {
                get {
                    return srcEnum.Current.Value;
                }
            }
            #endregion

            #region IDisposable Members
            void IDisposable.Dispose() {
                srcEnum.Dispose();
            }
            #endregion

            #region IEnumerator Members
            object System.Collections.IEnumerator.Current {
                get {
                    return srcEnum.Current.Value;
                }
            }

            bool System.Collections.IEnumerator.MoveNext() {
                return srcEnum.MoveNext();
            }

            void System.Collections.IEnumerator.Reset() {
                srcEnum.Reset();
            }
            #endregion

            #region IEnumerable<string> Members
            IEnumerator<string> IEnumerable<string>.GetEnumerator() {
                return new DictToEnum(src);
            }

            #endregion

            #region IEnumerable Members
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
                return new DictToEnum(src);
            }
            #endregion
        };

        private SortedDictionary<string, string> list;

        public IEnumerable<string> FilteredFiles {
            get {
                return new DictToEnum(list);
            }
        }


        public ListFilter(IFileProcessor _parent)
            : base(_parent) {
            this.list = new SortedDictionary<string, string>();
        }

        public override void ProcessFile(string fullPath) {
            string mod = fullPath.ToLower();
            if (!list.ContainsKey(mod)) {
                this.list[mod] = fullPath;

                base.ProcessFile(fullPath);
            }
        }

    }
}
