using System;
using System.Collections.Generic;
using System.Text;

using SevenZip;
using System.IO;

/*
    I've made a small change to SevenZipSharp that lets the compression continue even when an input
    file is unreadable (locked). It's in ArchiveUpdateCallback::GetStream(), it won't return -1 on error.
*/
namespace FlexiUp.Processors {

    class PackTo7Zip : FileProcessorDecorator {
        private string archivePath;
        //private Dictionary<string, FileRecord> files = new Dictionary<string, FileRecord>();
        private SortedSet<string> files = new SortedSet<string>();
        private string password;
        private long totalSize = 0;

        private IProgressSink progressSink = new NullProgressSink();
        public IProgressSink ProgressSink {
            get {
                return progressSink;
            }
            set {
                progressSink = value;
                if (value == null)
                    progressSink = new NullProgressSink();
            }
        }

        public PackTo7Zip(string _archivePath, string _password, IFileProcessor parent): base(parent) {
            archivePath = _archivePath;
            password = _password;
        }

        public override void ProcessFile(string fullPath) {
            files.Add(fullPath);

            totalSize += new FileInfo(fullPath).Length;
        }

        public override void Finish()
        {
            this.ProgressSink.Message(string.Format("Celkov� velikost: {0} MB", totalSize / 1024 / 1024));
            SevenZipCompressor cmp = new SevenZipCompressor();
            try
            {
                doneCount = 0;

                // add all the files
                SevenZipCompressor.SetLibraryPath(FlexiUp.Properties.Settings.Default.path_7z);
                cmp.EncryptHeaders = true;
                cmp.CompressionMethod = CompressionMethod.Lzma;
                cmp.CompressionMode = CompressionMode.Create;
                cmp.FileCompressionFinished += new EventHandler<EventArgs>(cmp_FileCompressionFinished);

                // make a copy
                List<string> fileNames = new List<string>(files);
                cmp.CompressFilesEncrypted(archivePath, 0, password, fileNames.ToArray());

            }
            catch (ExceptionList elist)
            {
                var excs = elist.Exceptions;
                if (excs.Count > 0)
                {
                    foreach (Exception e in excs)
                    {
                        string fileName = (string)(e.Data.Contains("FileName") ? e.Data["FileName"] : null);
                        files.Remove(fileName);
                        ProgressSink.Warning("Soubor preskocen: " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                ProgressSink.Error("Zalohovani selhalo: " + e.Message);
                throw;
            }

            // now log all the files which didn't have errors
            // TODO: only if didn't have a big archive-cancelling exception
            // TODO: um, this way I won't have files.log in the archive ...
            //      I guess can just add files, using CompressFilesEncrypted ...
            //      maybe for now not so important

            SendFilesToLog();
        }

        private void SendFilesToLog()
        {
            foreach (string f in files)
            {
                base.ProcessFile(f);
            }
            base.Finish();
        }

        private int doneCount;
        private void cmp_FileCompressionFinished(object sender, EventArgs e) {
            doneCount += 1;
            progressSink.Progress = doneCount / (float)files.Count;
        }

    }
}
