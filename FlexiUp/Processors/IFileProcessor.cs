using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Processors {
    /// <summary>
    /// Does something with a file supposed to be backed up.
    /// </summary>
    public interface IFileProcessor {
        void ProcessFile(string fullPath);
        void Finish();
    }
}
