﻿using FlexiUp.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Text;

namespace FlexiUp
{
    class HistoryDb: IDisposable
    {
        private SQLiteConnection conn;

        private const string HISTORYDB_FILENAME = "history.db";

        public static HistoryDb Open(string dbFolder)
        {
            return new HistoryDb(Path.Combine(dbFolder, HISTORYDB_FILENAME));
        }

        public HistoryDb(string dbPath)
        {
            DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(dbPath));
            if (!di.Exists) di.Create();

            SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder();
            builder.DataSource = dbPath;
            conn = new SQLiteConnection(builder.ToString());
            conn.Open();

            var createCmd = BackupDone.CreateTableCmd;
            createCmd.Connection = conn;
            createCmd.ExecuteNonQuery();

        }

        public void Dispose()
        {
            if (conn != null) conn.Close();
            conn = null;
        }

        public void InsertBackup(BackupDone obj)
        {
            var cmd = BackupDone.MakeInsertCmd(obj);
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
        }

        public IEnumerable<BackupDone> SelectBackups()
        {
            var cmd = BackupDone.MakeSelectCmd();
            cmd.Connection = conn;
            var reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    yield return BackupDone.FromReader(reader);
                }
            }
        }
    }
}
