using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
using FlexiUp.Processors;
using System.IO;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_LogMaker {
        private LogMaker fp;
        private string myDir;
/*        private Stream stream;
        private TextReader reader;*/
        private TextWriter writer;
        private const string lineEnd = "\r\n";

        [SetUp]
        public void setUp() {
            /*this.stream = new MemoryStream();
            this.reader = new Stream
             * */
            this.writer = new StringWriter();
            this.fp = new LogMaker(null, this.writer);
            this.myDir = FlexiUpApp.TestDir + "test_LogMaker\\";
        }

        [Test]
        public void test_basic() {
            this.fp.ProcessFile(this.myDir + "file1.txt");

            Assert.AreEqual(String.Format("{0}{4}{1}{4}{2}{3}", this.myDir + "file1.txt", "3", (new DateTime(2007, 9, 8, 13, 49, 21)).ToString(), lineEnd, LogMaker.Separator), this.writer.ToString());
        }
    }
}
