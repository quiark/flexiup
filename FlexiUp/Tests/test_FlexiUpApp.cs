using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
using System.IO;
using FlexiUp.Processors;
using System.Collections;

namespace FlexiUp.Tests {
    
    [TestFixture]
    public class test_FlexiUpApp {
        protected string myDir;
        protected ListMaker list;
        
        [SetUp]
        public void setUp() {
            this.myDir = FlexiUpApp.TestDir + "test_FlexiUpApp\\";

        }

        [Test]
        public void test_DeleteDirectory() {
            string testDir = this.myDir + "test_DeleteDirectory\\";

            // create the files and directories
            Directory.CreateDirectory(testDir + "target");
            using (StreamWriter sw = File.CreateText(testDir + "target\\file.txt")) {
                sw.WriteLine("hi");
            }
            File.SetAttributes(testDir + "target\\file.txt", FileAttributes.ReadOnly);

            Directory.CreateDirectory(testDir + "target\\dir");
            using (StreamWriter sw = File.CreateText(testDir + "target\\dir\\file.txt")) {
                sw.WriteLine("hi");
            }

            File.SetAttributes(testDir + "target\\dir\\file.txt", FileAttributes.ReadOnly);

            FlexiUpApp.Instance.DeleteDirectory(testDir + "target");
            Assert.IsFalse(Directory.Exists(testDir + "target"));
        }

    }
}
