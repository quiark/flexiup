using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using NUnit.Framework;

using FlexiUp.Processors;
using System.Threading;
using System.ComponentModel;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_CopyFileProcessor {
        private IFileProcessor fp;
        private string myDir;
        private string myTargetDir;

        [SetUp]
        public void setUp() {
            this.fp = new CopyFileProcessor(FlexiUpApp.TestDir + "test_CopyFileProcessor\\target", null);
            this.myDir = FlexiUpApp.TestDir + "test_CopyFileProcessor\\";
            this.myTargetDir = this.myDir + "target\\c_\\devel\\projects\\flexiup\\testdata\\test_CopyFileProcessor\\src\\";
            try {
                Directory.Delete(this.myDir + "target", true);
            } catch (Exception) {

            }
        }

        [TearDown]
        public void tearDown() {
            if (this.fp != null)
                this.fp.Finish();
        }

        [Test]
        public void test_classExists() {
            IFileProcessor fp = new CopyFileProcessor(FlexiUpApp.TestDir + "test_CopyFileProcessor\\target", null);
            fp.Finish();
        }

        [Test]
        public void test_ProcessFile() {
            this.fp.ProcessFile(this.myDir + "src\\file1.txt");
            this.fp.Finish();

            Assert.IsTrue(File.Exists(this.myTargetDir + "file1.txt"));
        }

        [Test]
        public void spike_thread() {
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += new DoWorkEventHandler(delegate(object sender, DoWorkEventArgs e) {
                for (int i = 1; i <= 100; i++) {
                    i = i + 1;
                }
            });

            bg.RunWorkerAsync();
        }
    }
}
