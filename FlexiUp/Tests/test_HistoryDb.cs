﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexiUp.Tests
{
    [TestFixture]
    class test_HistoryDb
    {
        private string myDir;

        [SetUp]
        public void setUp() {
            myDir = FlexiUpApp.TestDir + "test_HistoryDb\\";
        }

        [Test]
        public void test_basic()
        {
            HistoryDb db = new HistoryDb(myDir + "backupdb.db");
            db.InsertBackup(new Models.BackupDone
            {
                Date = DateTime.UtcNow,
                LogFile = "pepa",
                ModeUsed = Models.BackupDone.Mode.Complete,
                Size = 1230
            });

            foreach (var i in db.SelectBackups())
            {
                Console.WriteLine(i.Date);
            }
        }
    }
}
