using System;
using System.Collections.Generic;
using System.Text;
using FlexiUp.Processors;

using NUnit.Framework;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_ListMaker{
        private ListMaker fp;

        [SetUp]
        public void setUp() {
            this.fp = new ListMaker(null);
        }

        [Test]
        public void test_addSomeFiles() {
            this.fp.ProcessFile("path1");
            this.fp.ProcessFile("path2");
            this.fp.ProcessFile("PATH1");

            Assert.AreEqual(2, this.fp.FilesList.Count);
            Assert.IsTrue(this.fp.FilesList.Contains("path1"));
            Assert.IsTrue(this.fp.FilesList.Contains("path2"));
        }

        [Test]
        public void test_decorator() {
            ListMaker lm2 = new ListMaker(this.fp);

            lm2.ProcessFile("path1");

            Assert.IsTrue(this.fp.FilesList.Contains("path1"));
        }
    }
}
