using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
using FlexiUp.Processors;
using System.IO;
using System.Diagnostics;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_BackupTask {
        protected string myDir;
        protected ListMaker list;
        protected BackupTask backup;
        private PathBuilder pathBuilder = new PathBuilder("test_BackupTask");

        [SetUp]
        public void setUp() {
            myDir = FlexiUpApp.TestDir + "test_BackupTask\\";
            list = new ListMaker(null);
            backup = new BackupTask();
            
        }

        [Test]
        public void test_BackupFiles() {
            pathBuilder.TestName = "test_BackupFiles";
            backup.FileProcessor = list;
            backup.Start(this.myDir + "test_BackupFiles.bklist", pathBuilder.TestTarget);

            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "test_BackupFiles\\src\\file1.txt".ToLower()));
        }

        private bool stringBeginsWith(string testedStr, string beginning) {
            if (testedStr.Length < beginning.Length)
                return false;
            return testedStr.Substring(0, beginning.Length) == beginning;
        }

        [Test]
        public void test_copies() {
            pathBuilder.TestName = "test_copies";

            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);
            backup.Start(pathBuilder.GetTestSubPath("test_copies.bklist"), pathBuilder.TestTarget);

            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "file1.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "file2.txt")));

            using (StreamReader log = new StreamReader(Path.Combine(pathBuilder.TestTarget, "files.log"))) {
                Assert.IsTrue(stringBeginsWith(log.ReadLine(), Path.Combine(pathBuilder.GetTestSubPath("src"), "file1.txt")));
                Assert.IsTrue(stringBeginsWith(log.ReadLine(), Path.Combine(pathBuilder.GetTestSubPath("src"), "file2.txt")));
                
                Assert.IsTrue(log.EndOfStream);
            }
        }

        [Test]
        public void test_incrementalBackup() {
            pathBuilder.TestName = "test_incrementalBackup";
            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);

            List<string> logFiles = new List<string>();
            logFiles.Add(pathBuilder.GetTestSubPath("log1.log"));
            logFiles.Add(pathBuilder.GetTestSubPath("log2.log"));
            logFiles.Add(pathBuilder.GetTestSubPath("log3.log"));
            backup.Logs = logFiles;

            backup.Start(pathBuilder.GetTestSubPath("list.bklist"), pathBuilder.TestTarget);

            // now the asserts
            string targetDir = pathBuilder.TestExpandedTarget;
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file1.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(targetDir,"file6.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(targetDir,"file2.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(targetDir,"file3.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(targetDir,"file4.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(targetDir,"file5.txt")));
        }

        [Test]
        public void test_caseMaintained() {
            pathBuilder.TestName = "test_caseMaintained";
            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);

            backup.Start(pathBuilder.GetTestSubPath("list.bklist"), pathBuilder.TestTarget);
            DirectoryInfo dir = new DirectoryInfo(pathBuilder.TestExpandedTarget);

            bool found = false;
            foreach (FileInfo file in dir.GetFiles()) {
                if (file.Name == "fIlE.tXt")
                    found = true;
            }

            Assert.IsTrue(found);

            found = false;
            foreach (DirectoryInfo subdir in dir.GetDirectories()) {
                if (subdir.Name == "dIr")
                    found = true;
            }
            Assert.IsTrue(found);
        }

        [Test]
        public void test_EntryBackup() {
            pathBuilder.TestName = "test_EntryBackup";
            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);

            backup.Start(pathBuilder.GetTestSubPath("list.bklist"), pathBuilder.TestTarget);

            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "file1.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "file2.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir1\\file1.cpp")));
            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir1\\dir3\\file3.cpp")));
            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir1\\dir4\\file4.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir1\\dir4\\file6.dat")));
            Assert.IsFalse(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir1\\dir4\\file5.cpp")));
            Assert.IsFalse(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir2\\file4.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(pathBuilder.TestExpandedTarget, "dir2\\file5.txt")));
        }

        [Test]
        public void test_compress() {
            pathBuilder.TestName = "test_compress";
            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);

            backup.Compress = true;
            backup.Password = "abc";
            backup.Start(pathBuilder.GetTestSubPath("list.bklist"), Path.Combine(pathBuilder.TestTarget, "archive.7z"));

            test_PackTo7Zip.Check(pathBuilder, pathBuilder.GetTestSubPath(@"src\file1.txt"), pathBuilder.GetTestSubPath(@"src\file2.txt"), pathBuilder.GetTestSubPath(@"src\dir\file2.txt"), @"C:\files.log");

        }

        private class CheckErrorSink : NullProgressSink {
            public List<string> errors = new List<string>();


            public override void Error(string Message) {
                errors.Add(Message);
            }


        }

        [Test]
        public void test_reportError() {
            pathBuilder.TestName = "test_reportError";

            CheckErrorSink errorSink = new CheckErrorSink();

            using (FileStream lockFile = new FileStream(pathBuilder.GetTestSubPath("src\\file.txt"), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) {
                backup.Compress = false;
                backup.ProgressSink = errorSink;
                backup.Start(pathBuilder.GetTestSubPath("list.bklist"), pathBuilder.TestTarget);

                backup = new BackupTask();
                backup.Compress = true;
                backup.Password = "abc";
                backup.ProgressSink = errorSink;
                backup.Start(pathBuilder.GetTestSubPath("list.bklist"), pathBuilder.GetTestSubPath("target\\archive.7z"));
            }

            string errMsg = String.Format("The process cannot access the file '{0}' because it is being used by another process.", pathBuilder.GetTestSubPath("src\\file.txt"));
            Assert.AreEqual(errMsg, errorSink.errors[0]);
            Assert.AreEqual("Zalohovani selhalo: " + errMsg, errorSink.errors[1]);
            Assert.AreEqual(2, errorSink.errors.Count);
        }

        [Test]
        public void test_disableInc() {
            pathBuilder.TestName = "test_disableInc";

            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);

            List<string> logFiles = new List<string>();
            logFiles.Add(pathBuilder.GetTestSubPath("log1.log"));
            logFiles.Add(pathBuilder.GetTestSubPath("log2.log"));
            logFiles.Add(pathBuilder.GetTestSubPath("log3.log"));
            backup.Logs = logFiles;

            backup.Start(pathBuilder.GetTestSubPath("list.bklist"), pathBuilder.TestTarget);

            string targetDir = pathBuilder.TestExpandedTarget;
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file1.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file6.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file2.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file3.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file4.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(targetDir, "file5.txt")));

        }
    }
}
