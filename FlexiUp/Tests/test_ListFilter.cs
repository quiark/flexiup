using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
using FlexiUp.Processors;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_ListFilter {

        private class UniqueChecker: IFileProcessor {
            public List<string> got = new List<string>();

            public void ProcessFile(string fullPath) {
                MyAssert.NotContainsPath(fullPath, this.got);

                this.got.Add(fullPath.ToLower());
            }

            public void Finish() {
            }

        }

        private UniqueChecker checker;
        private ListFilter filter;

        [SetUp]
        public void setUp() {
            checker = new UniqueChecker();
            this.filter = new ListFilter(checker);
        }

        [Test]
        public void test_basic() {
            this.filter.ProcessFile("path1");
            this.filter.ProcessFile("path1");
            this.filter.ProcessFile("PAth1");
            this.filter.ProcessFile("path2");
            this.filter.Finish();

            MyAssert.ContainsPath("path1", checker.got);
            MyAssert.ContainsPath("path2", checker.got);
        }
    }
}
