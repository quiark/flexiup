using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;

using FlexiUp.Selectors;
using System.Xml;
using FlexiUp.Processors;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_AllSelector {

        protected AllSelector sel;
        protected string myDir;
        protected XmlDocument doc;
        protected ListMaker list;

        [SetUp]
        public void setUp() {
            this.myDir = FlexiUpApp.TestDir + "test_AllSelector\\";
            this.doc = new XmlDocument();

            this.list = new ListMaker(null);

            this.sel = new AllSelector();
            XmlNode initNode = this.doc.CreateElement("all");
            this.sel.Init(initNode);
        }

        [Test]
        public void test_basic() {
            XmlDocument doc = new XmlDocument();
            XmlNode entry1 = doc.CreateElement("entry");
            entry1.InnerText = this.myDir;

            this.sel.ProcessEntry(entry1, this.list);

            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file1.txt"));
            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "dir1\\file2.txt"));
        }

        [Test]
        public void test_onlyFile() {
            XmlDocument doc = new XmlDocument();
            XmlNode entry1 = doc.CreateElement("entry");
            entry1.InnerText = this.myDir + "file1.txt";

            this.sel.ProcessEntry(entry1, this.list);

            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file1.txt"));
            // TODO tady beru jen jeden soubor

        }

        [Test]
        public void test_moreEntries() {
            XmlDocument doc = new XmlDocument();
            XmlNode entry1 = doc.CreateElement("entry");
            StringBuilder str = new StringBuilder();
            str.AppendLine(this.myDir + "file1.txt");
            str.AppendLine(this.myDir + "file2.txt");
            str.AppendLine(this.myDir + "file3.txt");
            str.AppendLine(this.myDir + "dir1");
            entry1.InnerText = str.ToString();

            this.sel.ProcessEntry(entry1, this.list);

            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file1.txt"));
            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file2.txt"));
            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file3.txt"));
            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "dir1\\file2.txt"));

        }

    }
}
