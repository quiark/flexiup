using System;
using System.Collections.Generic;
using System.Text;

using FlexiUp.Scripting;
using NUnit.Framework;
using System.IO;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_InfoReader {
        MemoryStream src;
        StreamWriter writer;


        private void initStream() {
            src = new MemoryStream();
            writer = new StreamWriter(src);

        }


        [Test]
        public void test_basic() {
            // init the source data
            initStream();
            writer.WriteLine("list file=file.bklist");
            writer.WriteLine("incremental = true");
            writer.WriteLine("incremental log = log1.log");
            writer.WriteLine("target dir=c:\\");
            writer.Flush();
            src.Seek(0, SeekOrigin.Begin);

            InfoReader ir = new InfoReader();
            BackupInfo bi = ir.ReadStream(src);
            Assert.AreEqual("file.bklist", bi.ListFile);
            Assert.AreEqual("log1.log", bi.IncrementalLogFiles[0]);
            Assert.AreEqual("c:\\", bi.TargetDir);
        }

        [Test]
        public void test_expansion() {
            initStream();
            writer.WriteLine("target dir=C:\\Backup\\$(today)");
            writer.WriteLine("incremental log=$(today)$(today)end");
            writer.WriteLine("incremental log=begin$(today)$(today)");
            writer.WriteLine("incremental log=begin$(today)middle$(today)");
            writer.Flush();
            src.Seek(0, SeekOrigin.Begin);

            InfoReader ir = new InfoReader();
            BackupInfo bi = ir.ReadStream(src);
            string today = DateTime.Now.ToString("dd-MM-yyyy--hhmm");
            Assert.AreEqual("C:\\Backup\\" + today, bi.TargetDir);
            MyAssert.Contains<string>(today + today + "end", bi.IncrementalLogFiles);
            MyAssert.Contains<string>("begin" + today + today, bi.IncrementalLogFiles);
            MyAssert.Contains<string>("begin" + today + "middle" + today, bi.IncrementalLogFiles);
        }
    }
}
