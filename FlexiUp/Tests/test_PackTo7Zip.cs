using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

using SevenZip;
using System.IO;
using System.Diagnostics;
using FlexiUp.Processors;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_PackTo7Zip {
        PathBuilder pathBuilder = new PathBuilder("test_PackTo7Zip");

        internal static void Check(PathBuilder pathBuilder, params string[] paths) {
            Process unzip = Process.Start(FlexiUp.Properties.Settings.Default.zip_path, String.Format("x -pabc {0} -o{1}", Path.Combine(pathBuilder.TestTarget, "archive.7z"), pathBuilder.TestTarget));
            unzip.WaitForExit();

            // check
            foreach (string path in paths) {
                string file = pathBuilder.GetExpandedTarget(path, pathBuilder.TestTarget).Replace("C_", "C");
                Assert.IsTrue(File.Exists(file), file);

                Assert.AreEqual(File.ReadAllBytes(path), File.ReadAllBytes(file));
            }
        }

        [Test]
        public void test_SevenZipWorks() {
            pathBuilder.TestName = "test_SevenZipWorks";

            // cleanup
            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);
            Directory.CreateDirectory(pathBuilder.TestTarget);

            SevenZipCompressor cmp = new SevenZipCompressor();
            SevenZipCompressor.SetLibraryPath(@"7za.dll");
            
            cmp.CompressionMethod = CompressionMethod.Lzma;
            cmp.CompressionMode = CompressionMode.Create;
            cmp.DirectoryStructure = true;
            cmp.PreserveDirectoryRoot = true;
            cmp.EncryptHeaders = true;
            cmp.CompressFilesEncrypted(Path.Combine(pathBuilder.TestTarget, "archive.7z"), 0, "abc", pathBuilder.GetTestSubPath(@"dir\file1.txt"), pathBuilder.GetTestSubPath(@"dir\dir2\file2.txt"));

            Check(pathBuilder, pathBuilder.GetTestSubPath(@"dir\file1.txt"), pathBuilder.GetTestSubPath(@"dir\dir2\file2.txt"));

        }

        [Test]
        public void test_basic() {
            pathBuilder.TestName = "test_basic";

            // cleanup
            FlexiUpApp.Instance.DeleteDirectory(pathBuilder.TestTarget);
            Directory.CreateDirectory(pathBuilder.TestTarget);

            PackTo7Zip pack = new PackTo7Zip(Path.Combine(pathBuilder.TestTarget, "archive.7z"), "abc", null);
            pack.ProcessFile(pathBuilder.GetTestSubPath(@"dir\file1.txt"));
            pack.ProcessFile(pathBuilder.GetTestSubPath(@"dir\dir2\file2.txt"));
            pack.Finish();

            Check(pathBuilder, pathBuilder.GetTestSubPath(@"dir\file1.txt"), pathBuilder.GetTestSubPath(@"dir\dir2\file2.txt"));
        }

        [Test]
        public void test_locked_file_skipped()
        {
            pathBuilder.SetTestNameAndClean("test_locked_file_skipped");
            
            PackTo7Zip pack = new PackTo7Zip(Path.Combine(pathBuilder.TestTarget, "archive.7z"), "abc", null);
            pack.ProcessFile(@"C:\pagefile.sys");
            pack.ProcessFile(pathBuilder.GetTestSubPath("file1.txt"));
            pack.Finish();

            Check(pathBuilder, pathBuilder.GetTestSubPath(@"file1.txt"));
        }

        [Test]
        public void test_locked_file_not_logged()
        {
            // reuse data
            pathBuilder.SetTestNameAndClean("test_locked_file_skipped");

            ListMaker log = new ListMaker(null);
            PackTo7Zip pack = new PackTo7Zip(Path.Combine(pathBuilder.TestTarget, "archive.7z"), "abc", log);
            pack.ProcessFile(@"C:\pagefile.sys");
            pack.Finish();

            Assert.False(log.FilesList.Contains(@"C:\pagefile.sys"), "unreadable file should not be reported");
        }
    }
}
