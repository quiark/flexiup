using System;
using System.Xml;

using NUnit.Framework;
using FlexiUp.Selectors;
using FlexiUp.Processors;
using System.IO;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_FileMaskSelector {
        FileMaskSelector sel;
        string myDir;
        XmlDocument doc;
        ListMaker list;

        XmlElement init;
        XmlElement entry1;

        [SetUp]
        public void setUp() {
            this.sel = new FileMaskSelector();
            this.doc = new XmlDocument();
            this.myDir = FlexiUpApp.TestDir + "test_FileMaskSelector\\";
            this.list = new ListMaker(null);
            
            init = this.doc.CreateElement("init");
            //init.InnerText = @".*\.txt";
            XmlAttribute modeAttr = this.doc.CreateAttribute("mode");
            modeAttr.Value = "exclude";
            init.Attributes.Append(modeAttr);
            
            entry1 = this.doc.CreateElement("entry");
            entry1.InnerText = this.myDir;
        }

        [Test]
        public void test_basic() {
            this.init.InnerText = @".*\.txt";
            this.init.Attributes["mode"].Value = "exclude";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.ContainsPath(this.myDir + "file2.dat", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "file4.doc", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "dir1\\file2.dat", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file1.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file3.txt", this.list.FilesList);
        }

        [Test]
        public void test_include() {
            this.init.InnerText = @".*\.txt";
            this.init.Attributes["mode"].Value = "include";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.ContainsPath(this.myDir + "file1.txt", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "file3.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file2.dat", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file4.doc", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "dir1\\file1.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "dir1\\file2.dat", this.list.FilesList);
        }

        [Test]
        public void test_excludeDirs() {
            this.init.Attributes["mode"].Value = "exclude";
            this.init.InnerText = @"dir1";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.ContainsPath(this.myDir + "file1.txt", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "file2.dat", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "dir1\\file1.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "dir1\\file2.dat", this.list.FilesList);
        }

        [Test]
        public void test_matchOnlyFileName() {
            this.init.Attributes["mode"].Value = "exclude";
            this.init.InnerText = @"FileMaskSelector";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.ContainsPath(this.myDir + "file1.txt", this.list.FilesList);
        }

        [Test]
        public void test_caseInsensitivity() {
            this.init.Attributes["mode"].Value = "include";
            this.init.InnerText = @"FILE1.TXT";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.ContainsPath(this.myDir + "file1.txt", this.list.FilesList);

        }

        [Test]
        public void test_multipleRegexes() {
            this.init.Attributes["mode"].Value = "exclude";
            this.init.InnerText = ".*\\.txt;.*\\.dat\n.*\\.doc";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.NotContainsPath(this.myDir + "file1.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file2.dat", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file4.doc", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "dir1\\file1.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "dir1\\file2.dat", this.list.FilesList);
        }

        [Test]
        public void spike_GetFileName() {
            string x = Path.GetFileName("C:\\Temp");
        }

        [Test]
        public void test_trimRegexes() {
            this.init.InnerText = "   .*\\.txt  \n";
            this.init.Attributes["mode"].Value = "exclude";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            MyAssert.ContainsPath(this.myDir + "file2.dat", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "file4.doc", this.list.FilesList);
            MyAssert.ContainsPath(this.myDir + "dir1\\file2.dat", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file1.txt", this.list.FilesList);
            MyAssert.NotContainsPath(this.myDir + "file3.txt", this.list.FilesList);

        }

        [Test]
        public void test_bug_empty_regex() {
            this.init.InnerText = "   ";
            this.init.Attributes["mode"].Value = "include";

            this.sel.Init(this.init);
            this.sel.ProcessEntry(this.entry1, this.list);

            Assert.AreEqual(0, this.list.FilesList.Count);
        }
    }
}