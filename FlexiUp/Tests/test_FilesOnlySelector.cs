using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
using FlexiUp.Selectors;
using FlexiUp.Processors;
using System.Xml;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_FilesOnlySelector {
        private string myDir = FlexiUpApp.TestDir + "test_FilesOnlySelector\\";
        private FilesOnlySelector sel;
        private ListMaker list;
        private XmlDocument doc;

        [SetUp]
        public void setUp() {
            this.sel = new FilesOnlySelector();

            this.doc = new XmlDocument();
            this.list = new ListMaker(null);

        }

        [Test]
        public void test_basic() {
            XmlNode entry1 = this.doc.CreateElement("entry");
            StringBuilder str = new StringBuilder();
            str.AppendLine(this.myDir);
            entry1.InnerText = str.ToString();

            this.sel.ProcessEntry(entry1, this.list);

            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file1.txt"));
            Assert.IsTrue(this.list.FilesList.Contains(this.myDir.ToLower() + "file2.txt"));
            Assert.IsFalse(this.list.FilesList.Contains(this.myDir.ToLower() + "dir1\\file2.txt"));

        }
    }
}
