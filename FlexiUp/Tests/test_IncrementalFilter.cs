using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
using FlexiUp.Processors;

namespace FlexiUp.Tests {
    [TestFixture]
    public class test_IncrementalFilter {

        private IncrementalFilter filter;
        private string myDir;
        private ListMaker list;

        [SetUp]
        public void setUp() {
            this.myDir = FlexiUpApp.TestDir + "test_IncrementalFilter\\";
            this.list = new ListMaker(null);
        }

        [Test]
        public void test_basic() {
            string testDir = this.myDir + "test_basic\\";
            
            this.filter = new IncrementalFilter(this.list, this.myDir + "test_basic\\log1.log");

            Assert.IsFalse(this.filter.Passed(testDir + "file1.txt"));
            Assert.IsTrue(this.filter.Passed(testDir + "file2.txt"));
            Assert.IsTrue(this.filter.Passed(testDir + "sizechanged.txt"));
            Assert.IsTrue(this.filter.Passed(testDir + "changed.txt"));
        }

        /*
        [Test]
        public void test_disabled() {
            string testDir = myDir + "test_disabled\\";
            filter = new IncrementalFilter(list, testDir + "log.log", delegate(string fullPath) {
                return false;
            });

            Assert.IsTrue(filter.Passed(testDir + "file.txt"));
        }
        */
    }
}
