using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using FlexiUp.Processors;

namespace FlexiUp.EntryProcessors {

    public interface IReceivesBklistEntries {
        void Process(string use, XmlNode entry);
        void Finish();
    }

}
