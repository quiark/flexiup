using System;
using System.Collections.Generic;
using System.Text;
using FlexiUp.Processors;
using System.Xml;
using FlexiUp.Selectors;

namespace FlexiUp.EntryProcessors {
    /// <summary>
    /// This class receives entries from BkListParser, makes a list of them
    /// and lets each entry be processed by an ISelector. When the selector
    /// encounters another directory, it lets this class decide what to do.
    /// OverridableSelectorDispatch looks that directory up in the entry list and if a different
    /// selector is found for that directory, it lets the new selector process
    /// the directory.
    /// 
    /// This allows for overriding selectors in subdirectories.
    /// 
    /// </summary>
    class OverridableSelectorDispatch : IReceivesBklistEntries {
        protected IFileProcessor processor;
        protected SelectorDict selectors;
        
        /// <summary>A directory-selector map of directory-processing entries</summary>
        protected Dictionary<string, ISelector> dirSelectorsMap;
        /// <summary>A node-selector map of all entries</summary>
        protected Dictionary<XmlNode, ISelector> allSelectorsMap;

        private ISelector currentSelector;
        public ISelector CurrentSelector {
            get {
                return currentSelector;
            }
        }

        private IProgressSink progressSink = new NullProgressSink();
        public IProgressSink ProgressSink {
            get {
                return progressSink;
            }
            set {
                progressSink = value;
                if (progressSink == null)
                    progressSink = new NullProgressSink();
            }
        }

        public OverridableSelectorDispatch(IFileProcessor fp, SelectorDict dict) {
            dirSelectorsMap = new Dictionary<string, ISelector>();
            allSelectorsMap = new Dictionary<XmlNode, ISelector>();

            processor = fp;
            selectors = dict;
        }

        /// <summary>
        /// Called by selectors when entering a new directory. 
        /// Decides if the directory is handled by a different selector.
        /// </summary>
        protected bool directoryEncountered(ISelector current, string path) {
            string lowPath = path.ToLower();
            if (!dirSelectorsMap.ContainsKey(lowPath))
                return true;

            ISelector sel = dirSelectorsMap[lowPath];
            if (sel == current)
                return true;

            return false;
        }

        /// <summary>
        /// Runs the selector on the entry
        /// </summary>
        private void myProcessEntry(XmlNode entry, ISelector selector) {
            currentSelector = selector;
            selector.DirectoryEncounteredHandler = new DirectoryEncounteredDelegate(directoryEncountered);
            selector.ProcessEntry(entry, processor);
        }

        #region IReceivesBklistEntries Members

        /// <summary>
        /// Stores the directories to back up for later. Actual work is done at the
        /// end, in Finish().
        /// </summary>
        void IReceivesBklistEntries.Process(string use, XmlNode entry) {
            ISelector sel = selectors[use];
            allSelectorsMap.Add(entry, sel);

            IList<string> dirs = sel.GetDirectoryList(entry);
            if (dirs != null) {
                foreach (string dir in dirs) {
                    dirSelectorsMap.Add(dir.ToLower(), sel);
                }
            }

        }

        /// <summary>
        /// Walks stored entries and backups them.
        /// </summary>
        void IReceivesBklistEntries.Finish() {
            int counter = 0;
            foreach (XmlNode entry in allSelectorsMap.Keys) {
                myProcessEntry(entry, allSelectorsMap[entry]);
                
                counter++;
                progressSink.Progress = counter / (float)allSelectorsMap.Keys.Count;
            }
        }


        #endregion
    }
}
