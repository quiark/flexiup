using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Selectors {
    /// <summary>
    /// Takes only the files inside a directory and does not go deeper into
    /// directories.
    /// </summary>
    public class FilesOnlySelector : AllSelector {
        protected override void dirFound(string fullPath, bool first) {
            if (first)
                base.dirFound(fullPath, first);
        }
    }
}
