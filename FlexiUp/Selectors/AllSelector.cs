using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using FlexiUp.Processors;


namespace FlexiUp.Selectors {
    public class AllSelector : ISelector {
        protected IFileProcessor output;
        protected DirectoryEncounteredDelegate _directoryEncounteredHandler;
        private bool incFilterEnabled;

        public const string INC_ENABLED_ATTR = "incFilterEnabled";

        public AllSelector() {
            incFilterEnabled = true;
        }

        protected void walkDirectory(string path) {
            DirectoryInfo dir = new DirectoryInfo(path);
            foreach (DirectoryInfo d in dir.GetDirectories()) {
                this.internalDirFound(d.FullName, false);
            }

            foreach (FileInfo f in dir.GetFiles()) {
                // do the file processing here
                this.fileFound(f);
            }
            
        }

        protected virtual void fileFound(FileInfo file) {
            this.output.ProcessFile(file.FullName);
        }

        protected virtual void dirFound(string fullPath, bool first) {
            this.walkDirectory(fullPath);
        }

        /// <summary>
        /// Consults the DirectoryEnteredDelegate if this directory should
        /// be entered.
        /// </summary>
        /// <param name="first">If this is the toplevel directory, the one entered in the entry.</param>
        private void internalDirFound(string fullPath, bool first) {
            if (_directoryEncounteredHandler != null) {
                if (!_directoryEncounteredHandler(this, fullPath))
                    return;
            }

            // ok, let's go in
            dirFound(fullPath, first);
        }

        #region ISelector Members

        public virtual void Init(System.Xml.XmlNode node) {
            XmlAttribute incEnabledAttr = node.Attributes.GetNamedItem(INC_ENABLED_ATTR) as XmlAttribute;
            if ((incEnabledAttr != null) && (Boolean.Parse(incEnabledAttr.Value) == false) ) {
                incFilterEnabled = false;
            }
        }

        public void ProcessEntry(System.Xml.XmlNode node, IFileProcessor _output) {
            this.output = _output;
            TextReader reader = new StringReader(node.InnerText);

            string line;
            while ((line = reader.ReadLine()) != null) {
                line = line.Trim();

                if (File.Exists(line)) {
                    this.fileFound(new FileInfo(line));
                } else if (Directory.Exists(line)) {
                    this.internalDirFound(line, true);
                }
            }

        }

        public virtual void WriteToXml(XmlWriter writer) {
            writer.WriteAttributeString(INC_ENABLED_ATTR, incFilterEnabled.ToString());
        }

        public IList<string> GetDirectoryList(XmlNode entry) {
            List<string> result = new List<string>();
            TextReader reader = new StringReader(entry.InnerText);

            string line;
            while ((line = reader.ReadLine()) != null) {
                line = line.Trim();
                if (Directory.Exists(line)) {
                    result.Add(line);
                }
            }

            return result;
            
        }

        public DirectoryEncounteredDelegate DirectoryEncounteredHandler {
            set {
                _directoryEncounteredHandler = value;
            }
        }

        public bool IncrementalFilterEnabled {
            get {
                return incFilterEnabled;
            }
        }

        #endregion
    }
}
