using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using FlexiUp.Processors;

namespace FlexiUp.Selectors {
    public delegate bool DirectoryEncounteredDelegate(ISelector current, String path);

    public interface ISelector {
        /// <summary>
        /// Reads configuration from XML
        /// </summary>
        void Init(XmlNode node);

        /// <summary>
        /// Handles one entry in the backup list
        /// </summary>
        /// <param name="node">The entry node</param>
        /// <param name="output">The output processor to use.</param>
        void ProcessEntry(XmlNode node, IFileProcessor output);

        /// <summary>
        /// Saves the configuration to XML.
        /// </summary>
        void WriteToXml(XmlWriter writer);

        /// <summary>
        /// Parses the node and returns the list of directories that will be backed
        /// up by this selector. If the implementation is some special selector,
        /// return null.
        /// </summary>
        IList<String> GetDirectoryList(XmlNode entry);

        /// <summary>
        /// If this delegate is set, it is called every time a new directory
        /// is entered. If it returns true, this selector continues entry
        /// processing. If it returns false, the selector skips that directory.
        /// </summary>
        DirectoryEncounteredDelegate DirectoryEncounteredHandler {
            set;
        }

        /// <summary>
        /// Returns false if incremental filter should be disabled for items
        /// generated by this selector.
        /// </summary>
        bool IncrementalFilterEnabled {
            get;
        }
    }
}
