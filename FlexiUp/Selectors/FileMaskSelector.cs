using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;


namespace FlexiUp.Selectors {
    /// <summary>
    /// Uses a regular expression to match a subset of files which
    /// will be either included as the only ones, or excluded from
    /// the set of all files.
    /// </summary>
    class FileMaskSelector : AllSelector {

        protected bool exclude;
        protected List<Regex> regexes;

        public FileMaskSelector() {
            regexes = new List<Regex>();
        }

        protected void addRegexLine(string line) {
            string [] wcards = line.Split(new char[] { ';' });

            foreach (string wc in wcards) {
                Regex re = new Regex(wc.Trim(), RegexOptions.IgnoreCase);
                this.regexes.Add(re);
            }
        }

        protected bool anyMatches(string str) {
            foreach (Regex re in this.regexes) {
                if (re.IsMatch(str))
                    return true;
            }

            return false;
        }

        protected override void fileFound(FileInfo file) {
            string str = file.Name;
            if (((this.exclude) && (!this.anyMatches(str))) ||
                ((!this.exclude) && (this.anyMatches(str)))
                )
                this.output.ProcessFile(file.FullName);
        }

        protected override void dirFound(string fullPath, bool first) {
            string str = Path.GetFileName(fullPath);
            if (this.exclude) {
                if (!this.anyMatches(str))
                    base.dirFound(fullPath, false);

            } else {
                // in include mode, include every directory
                base.dirFound(fullPath, false);
            }
        }

        #region ISelector Members

        public override void Init(System.Xml.XmlNode node) {
            base.Init(node);

            // read mode
            this.exclude = (node.Attributes["mode"].Value == "exclude");

            // read the lines
            TextReader reader = new StringReader(node.InnerText);
            string line;
            while ((line = reader.ReadLine()) != null) {
                line = line.Trim();
                if (String.IsNullOrEmpty(line))
                    continue;

                this.addRegexLine(line);
            }
        }

        public override void WriteToXml(XmlWriter writer) {
            base.WriteToXml(writer);

            writer.WriteStartAttribute("mode");
            if (this.exclude)
                writer.WriteValue("exclude");
            else
                writer.WriteValue("include");
            writer.WriteEndAttribute();

            writer.WriteString("\n");
            foreach (Regex re in this.regexes) {
                writer.WriteString(re.ToString() + "\n");
            }
            writer.WriteString("\n");

        }

        #endregion
    }
}
