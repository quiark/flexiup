﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;

namespace FlexiUp.Models
{
    public class BackupDone
    {
        public enum Mode {
            Complete = 1,
            Incremental = 2
        }

        public DateTime Date { get; set; }
        public long Size { get; set; }
        public string LogFile { get; set; }
        public Mode ModeUsed { get; set; }
        
        public static SQLiteCommand CreateTableCmd {
            get {
                return new SQLiteCommand(
                    @"CREATE TABLE IF NOT EXISTS backupdone (
                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        date TIMESTAMP,
                        size INTEGER,
                        logfile VARCHAR,
                        modeused INTEGER
                )");
            }
        }

        public static SQLiteCommand MakeInsertCmd(BackupDone data)
        {
            var result = new SQLiteCommand(@"INSERT INTO backupdone (date, size, logfile, modeused) VALUES (@date, @size, @logfile, @modeused)");
            result.Parameters.Add(new SQLiteParameter("@date", data.Date));
            result.Parameters.Add(new SQLiteParameter("@size", data.Size));
            result.Parameters.Add(new SQLiteParameter("@logfile", data.LogFile));
            result.Parameters.Add(new SQLiteParameter("@modeused", data.ModeUsed));
            return result;
        }

        public static SQLiteCommand MakeSelectCmd()
        {
            var result = new SQLiteCommand(@"SELECT date, size, logfile, modeused FROM backupdone ORDER BY date DESC");
            return result;
        }

        public static BackupDone FromReader(SQLiteDataReader reader)
        {
            return new BackupDone
            {
                Date = reader.GetDateTime(0),
                Size = reader.GetInt32(1),
                LogFile = reader.GetString(2),
                ModeUsed = (reader.GetInt32(3) == 1) ? Mode.Complete : Mode.Incremental
            };
        }
    }
}
