using System;
using System.Collections.Generic;
using System.Text;
using FlexiUp.Processors;
using System.Xml;
using System.IO;
using System.Collections;
using FlexiUp.EntryProcessors;

namespace FlexiUp {

    public class FlexiUpApp {
        public const string TestDir = "C:\\Devel\\Projects\\FlexiUp\\testData\\";
        public const string DataDir = "C:\\Devel\\Projects\\FlexiUp\\Dist\\";

        private static FlexiUpApp myInstance;
        public static FlexiUpApp Instance {
            get {
                if (myInstance == null)
                    myInstance = new FlexiUpApp();

                return myInstance;
            }
        }

        public void DeleteDirectory(string path) {
            try {
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (FileInfo file in dir.GetFiles()) {
                    file.Attributes = FileAttributes.Normal;
                    file.Delete();
                }

                foreach (DirectoryInfo subdir in dir.GetDirectories()) {
                    this.DeleteDirectory(subdir.FullName);
                }

                dir.Delete(true);
            } catch (Exception) {

            }
        }

    }
}
