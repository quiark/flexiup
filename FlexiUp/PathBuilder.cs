using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FlexiUp {
    /// <summary>
    /// Builds paths for test output, input using program installation dir, test case name, ...
    /// </summary>
    class PathBuilder {
        private string testFixtureName;
        private string programDir;
        private string testName;

        public PathBuilder(string _testFixtureName) {
            programDir = "C:\\Devel\\Projects\\FlexiUp";
            testFixtureName = _testFixtureName;
        }

        /// <summary>
        /// For use outside of tests.
        /// </summary>
        public PathBuilder() {

        }

        /// <summary>
        /// Set in each test method.
        /// </summary>
        public string TestName {
            get {
                return testName;
            }
            set {
                testName = value;
            }
        }

        public string TestDir {
            get {
                return Path.Combine(Path.Combine(Path.Combine(programDir, "testData"), testFixtureName), testName);
            }
        }

        public void SetTestNameAndClean(string testName)
        {
            TestName = testName;
            FlexiUpApp.Instance.DeleteDirectory(TestTarget);
            Directory.CreateDirectory(TestTarget);
        }

        public string GetExpandedTarget(string sourcePath, string targetPath) {
            return Path.Combine(targetPath, sourcePath.Replace(':', '_'));
        }

        public string GetTestSubPath(string name) {
            return Path.Combine(this.TestDir, name);
        }

        public string TestSrc {
            get {
                return GetTestSubPath("src");
            }
        }

        public string TestTarget {
            get {
                return GetTestSubPath("target");
            }
        }

        public string TestExpandedTarget {
            get {
                return GetExpandedTarget(TestSrc, TestTarget);
            }
        }
    }
}
