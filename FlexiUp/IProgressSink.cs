using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp {
    public interface IProgressSink {
        float Progress { set; }
        void Error(string Message);
        void Message(string Message);
        void Warning(string Message);
    }

    class NullProgressSink : IProgressSink{
        public float Progress {
            set {}
        }

        public virtual void Error(string Message) {}
        public virtual void Message(string Message) {}
        public virtual void Warning(string Message) {}
    }
}
