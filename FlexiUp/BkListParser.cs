using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using FlexiUp.Selectors;
using FlexiUp.Processors;
using FlexiUp.EntryProcessors;

namespace FlexiUp {
    /// <summary>
    /// Reads a bklist input file, creates all selectors described there and passes the 
    /// contents to an IReceivesBklistEntries.
    /// </summary>
    public class BkListParser {

        protected XmlDocument doc;
        private XmlElement listNode;
        private XmlNodeList entriesList;
        public int EntriesCount {
            get {
                return entriesList.Count;
            }
        }

        public BkListParser(string path) {
            this.doc = new XmlDocument();
            this.doc.Load(path);

            listNode = this.doc.GetElementsByTagName("list")[0] as XmlElement;
            entriesList = listNode.ChildNodes;
        }

        protected ISelector getSelectorByName(string name) {
            switch (name) {
                case "AllSelector":
                    return new AllSelector();
                case "FileMaskSelector":
                    return new FileMaskSelector();
                case "FilesOnlySelector":
                    return new FilesOnlySelector();

            }

            return null;
        }

        public SelectorDict ReadInit() {
            SelectorDict res = new SelectorDict();
            XmlElement initElement = this.doc.GetElementsByTagName("init")[0] as XmlElement;
            if (initElement == null)
                return res;


            foreach (XmlNode el in initElement.ChildNodes) {
                string name = el.Name;

                ISelector newSel = this.getSelectorByName(el.Attributes["sel"].Value);
                newSel.Init(el);
                res.Add(name, newSel);
            }

            return res;
        }

        public void ProcessEntries(IReceivesBklistEntries receiver) {
            foreach (XmlNode node in entriesList) {
                string use = node.Attributes["use"].Value;

                receiver.Process(use, node);
            }

            receiver.Finish();
        }
    }
}
