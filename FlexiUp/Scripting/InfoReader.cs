using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace FlexiUp.Scripting {
    /// <summary>
    /// This class reads the information about a backup task from a file.
    /// </summary>
    public class InfoReader {

        private const string OPT_LIST_FILE = "list file";
        private const string OPT_INC_LOG = "incremental log";
        private const string OPT_TARGET_DIR = "target dir";
        private const string OPT_COMPRESS = "compress";
        private const string OPT_INC_DB = "incremental database";
        private const string OPT_PASSWORD = "password";

        /// <summary>
        /// Reads the information from a stream.
        /// </summary>
        /// <returns>A new BackupInfo object.</returns>
        public BackupInfo ReadStream(Stream src) {
            BackupInfo res = new BackupInfo();
            res.IncrementalLogFiles = new List<string>();

            using (StreamReader reader = new StreamReader(src)) {
                string line;
                while (null != (line = reader.ReadLine())) {
                    parseLine(res, line);
                }
            }

            return res;
        }

        /// <summary>
        /// Parses a single line from the configuration file and stores
        /// the information into <paramref name="info"/>.
        /// </summary>
        private void parseLine(BackupInfo info, string line) {
            string[] parts = line.Split('=');
            if (parts.Length < 2)
                throw new ParseException("Too few parameters", line);
            string name = parts[0].Trim().ToLower();
            string value = parts[1].Trim();

            handleEntry(info, name, value);
        }

        /// <summary>
        /// Performs the operation specific for the name, value pair.
        /// </summary>
        private void handleEntry(BackupInfo info, string name, string value) {
            if (name == OPT_LIST_FILE) {
                info.ListFile = value;
            } else if (name == OPT_INC_LOG) {
                info.IncrementalLogFiles.Add(expandMacros(value));
            } else if (name == OPT_TARGET_DIR) {
                info.TargetDir = expandMacros(value);
            } else if (name == OPT_COMPRESS) {
                info.Compress = bool.Parse(value);
            } else if (name == OPT_INC_DB) {
                info.IncrementalLogDb = expandMacros(value);
            } else if (name == OPT_PASSWORD) {
                /*
                Yes, we store the password in the config file unencrypted. My use case is that the script file is saved on
                the same disk where the backup data comes from so access to this password is as hard as access to the files
                themselves. Of course this password should not be used elsewhere because compromising this file compromises
                that other site too. Furthermore, users should be careful about copying the script files to anywhere.
                */
                info.Password = value;
            }
        }


        /// <summary>
        /// Expands all known macros in the inputString and returns
        /// the resulting string.
        /// 
        /// Each macro starts with a dollar sign ($) followed by
        /// parentheses containing the macro name or definition.
        /// For example $(today)
        /// </summary>
        private string expandMacros(string inputString) {
            return ExpandCustomMacros(inputString, performMacro);
        }

        public static string ExpandCustomMacros(string inputString, Func<string, string> expansionFn) {
            Regex macro_re = new Regex(@"\$\((\w+)\)");
            MatchCollection matches = macro_re.Matches(inputString);
            StringBuilder resultBuilder = new StringBuilder();
            int pos = 0;  // how many characters have been copied to the result

            foreach (Match m in matches) {
                // copy previous string verbatim
                resultBuilder.Append(inputString.Substring(pos, m.Index - pos));

                // get the value of the macro
                resultBuilder.Append(expansionFn(m.Groups[1].Value));

                // update position
                pos = m.Index + m.Length;
            }

            // append the final portion
            resultBuilder.Append(inputString.Substring(pos));

            return Environment.ExpandEnvironmentVariables(resultBuilder.ToString());
        }

        /// <summary>
        /// Returns the result of a macro
        /// </summary>
        private string performMacro(string macro) {
            if (macro == "today")
                return DateTime.Now.ToString("dd-MM-yyyy--hhmm");
            else
                // unchanged
                return string.Format("$({0})", macro);
        }
    }

}
