using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Scripting {
    public class ParseException : Exception {
        public ParseException(string message, string erroneousString) : base(message + " in " + erroneousString) {

        }
    }
}
