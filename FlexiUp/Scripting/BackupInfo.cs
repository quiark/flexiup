using System;
using System.Collections.Generic;
using System.Text;

namespace FlexiUp.Scripting {
    /// <summary>
    /// This class stores information about the settings of a backup.
    /// </summary>
    public class BackupInfo {
        public string ListFile;
        public IList<string> IncrementalLogFiles;
        public string TargetDir;
        public bool Compress;
        public string IncrementalLogDb;
        public string Password;
    }
}
