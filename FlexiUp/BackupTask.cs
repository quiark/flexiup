using System;
using System.Collections.Generic;
using System.Text;
using FlexiUp.Processors;
using FlexiUp.EntryProcessors;
using System.IO;

namespace FlexiUp {
    /// <summary>
    /// Manages the backup. Sets up the selectors and stuff, starts the process and by default
    /// also processes the files found by the selector.
    /// </summary>
    public class BackupTask : IFileProcessor{
#if DEBUG
        private const string DEFAULT_LOGPATH = "C:\\Temp\\files.log";
#else
        private const string DEFAULT_LOGPATH = "C:\\files.log";
#endif

        private ListFilter filter;

        private IFileProcessor myFileProcessor;
        public IFileProcessor FileProcessor {
            get {
                return myFileProcessor;
            }
            set {
                myFileProcessor = value;
            }
        }

        /// <summary>
        /// Backup logs to use for incremental backup.
        /// </summary>
        private List<string> logs = new List<string>();
        public List<string> Logs {
            get {
                return logs;
            }
            set {
                logs = value;
            }
        }



        private string password;
        public string Password {
            get {
                return password;
            }
            set {
                password = value;
            }
        }

        private bool compress = false;
        public bool Compress {
            get {
                return compress;
            }
            set {
                compress = value;
            }
        }

        private string target;
        private LogMaker log;
        private IFileProcessor saveFiles;
        private PackTo7Zip saveTo7Zip = null;
        private IncFilterGroup incFilterGroup;
        private OverridableSelectorDispatch dispatch;
        private BkListParser parser;
        private string logPath;
        private HistoryDb history;



#region Progress
        /// <summary>
        /// Receives information about the backup.
        /// </summary>
        private IProgressSink progressSink;

        public IProgressSink ProgressSink {
            get {
                return progressSink;
            }
            set {
                progressSink = value;
            }
        }

        public string IncrementalLogDb { get; internal set; }

        /// <summary>
        /// We have two sources of progress, each of them must be received by a IProgressSink. This class
        /// receives one source and sends it to the master IProgressSink. 
        /// </summary>
        private class ProgressPart : IProgressSink {
            private int partIndex;
            private int partsCount;
            private IProgressSink parent;

            /// <summary>
            /// Initializes a new instance of the class.
            /// </summary>
            /// <param name="_partIndex">Which part of the progress is this instance.</param>
            /// <param name="_partsCount">How many parts are there in total.</param>
            /// <param name="_parent">Master progress sink which the information is passed to.</param>
            public ProgressPart(int _partIndex, int _partsCount, IProgressSink _parent) {
                partIndex = _partIndex;
                partsCount = _partsCount;
                parent = _parent;
            }

            public float Progress {
                set { parent.Progress = ((value + partIndex) / (float)partsCount); }
            }

            public void Error(string Message) {
                parent.Error(Message);
            }

            public void Message(string Message) {
                parent.Message(Message);
            }

            public void Warning(string Message)
            {
                parent.Warning(Message);
            }

        }

#endregion

        public BackupTask() {
            filter = new ListFilter(this);
            myFileProcessor = filter;
        }

        public void Start(string bklistFileName, string _target) {
            target = _target;
            if (progressSink == null) {
                progressSink = new NullProgressSink();
            }

            DateTime now = DateTime.Now;

            progressSink.Message("Vypisuji soubory...");

            // read bklist
            parser = new BkListParser(bklistFileName);
            SelectorDict dict = parser.ReadInit();

            // prepare backup
            if (compress) {
                Directory.CreateDirectory(Path.GetDirectoryName(target));
                logPath = DEFAULT_LOGPATH;
                log = new LogMaker(null, new StreamWriter(logPath, false));
                saveTo7Zip = new PackTo7Zip(target, Password, log);
                saveTo7Zip.ProgressSink = new ProgressPart(1, 2, this.ProgressSink);
                saveFiles = saveTo7Zip;
            } else {
                Directory.CreateDirectory(target);
                log = new LogMaker(null, new StreamWriter(Path.Combine(target, "files.log")));
                saveFiles = new CopyFileProcessor(target, log);
            }

            incFilterGroup = new IncFilterGroup(logs);
            dispatch = new OverridableSelectorDispatch(FileProcessor, dict);
            dispatch.ProgressSink = new ProgressPart(0, 2, this.ProgressSink);
            
            // start backup
            parser.ProcessEntries(dispatch);

            // finish
            FileProcessor.Finish();

            // copy log file
            if (IncrementalLogDb != null)
            {
                // TODO: detect if backup was ok ... 
                string finalLogPath = Path.Combine(IncrementalLogDb, now.ToString("dd-MM-yyyy--hhmm") + ".log");
                File.Copy(logPath, finalLogPath);
                using (history = HistoryDb.Open(IncrementalLogDb)) {
                    history.InsertBackup(new Models.BackupDone
                    {
                        Date = now,
                        LogFile = finalLogPath,
                        Size = new FileInfo(target).Length,
                        ModeUsed = (logs.Count > 0) ? Models.BackupDone.Mode.Incremental : Models.BackupDone.Mode.Complete
                    });
                }
            }
        }

#region IFileProcessor Members

        public void ProcessFile(string fullPath) {
            if ((!dispatch.CurrentSelector.IncrementalFilterEnabled) || (incFilterGroup.IsModified(fullPath))) {
                try {
                    saveFiles.ProcessFile(fullPath);
                    //log.ProcessFile(fullPath);
                } catch (Exception e) {
                    progressSink.Error(e.Message);
                }
            }
        }

        public void Finish() {
            progressSink.Message("Kopiruji soubory...");

            /* currently empty
            if (logPath != null) {
                saveFiles.ProcessFile(logPath);
            }
            */
            saveFiles.Finish();

            progressSink.Message("Zaloha dokoncena.");
        }

#endregion

    }
}
