using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using FlexiUp.Processors;
using FlexiUp.Selectors;
using System.IO;
using FlexiUp.EntryProcessors;

namespace FlexiUp {
    public class BackupItemInfo {
        public string SelectorName;

        public BackupItemInfo(string _selectorName) {
            this.SelectorName = _selectorName;

        }
    }

    public class SelectedItems : Dictionary<string, BackupItemInfo> {
        

        #region ReadIntoListProcessor
        protected class ReadIntoListProcessor : IReceivesBklistEntries {
            private SelectedItems parent;

            public ReadIntoListProcessor(SelectedItems _parent) {
                this.parent = _parent;
            }

            public void Process(string use, XmlNode entry) {
                TextReader reader = new StringReader(entry.InnerText);

                string line;
                while ((line = reader.ReadLine()) != null) {
                    this.parent[line] = new BackupItemInfo(use);
                }

            }

            public void Finish() {
            }
        }
        #endregion


        public bool HasKey(string key) {
            BackupItemInfo v;
            return this.TryGetValue(key, out v);
        }

        #region Saving to XML
        public void SaveToXml(string fileName, SelectorDict selectorDict) {
            XmlWriterSettings cfg = new XmlWriterSettings();
            cfg.Indent = true;

            XmlWriter writer = XmlWriter.Create(fileName, cfg);
            writer.WriteStartDocument();
            writer.WriteStartElement("bklist");
            this.saveSelectors(writer, selectorDict);

            writer.WriteStartElement("list");
            foreach (string key in this.Keys) {
                writer.WriteStartElement("entry");

                writer.WriteStartAttribute("use");
                writer.WriteValue(this[key].SelectorName);
                writer.WriteEndAttribute();

                writer.WriteValue(key);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Close();

        }

        private void saveSelectors(XmlWriter writer, SelectorDict selectorDict) {
            writer.WriteStartElement("init");

            foreach (string key in selectorDict.Keys) {
                writer.WriteStartElement(key);

                writer.WriteStartAttribute("sel");
                writer.WriteValue(selectorDict[key].GetType().Name);
                selectorDict[key].WriteToXml(writer);

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
        #endregion

        #region Loading from XML
        public SelectorDict LoadFromXml(string fileName) {
            BkListParser parser = new BkListParser(fileName);
            SelectorDict res = parser.ReadInit();
            
            // read using the internal IReceivesBklistEntries
            parser.ProcessEntries(new ReadIntoListProcessor(this));

            return res;
        }
        #endregion
    }
}
