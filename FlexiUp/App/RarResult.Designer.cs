namespace FlexiUp.App {
    partial class ZipResult {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rarBtn = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.Label();
            this.passBox = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.targetDirBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDeleteWhenDone = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rarBtn
            // 
            this.rarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rarBtn.Location = new System.Drawing.Point(21, 125);
            this.rarBtn.Name = "rarBtn";
            this.rarBtn.Size = new System.Drawing.Size(162, 25);
            this.rarBtn.TabIndex = 0;
            this.rarBtn.Text = "Zabalit";
            this.rarBtn.UseVisualStyleBackColor = true;
            this.rarBtn.Click += new System.EventHandler(this.rarBtn_Click);
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Location = new System.Drawing.Point(6, 153);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(53, 13);
            this.status.TabIndex = 1;
            this.status.Text = "P�ipraven";
            // 
            // passBox
            // 
            this.passBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.passBox.Location = new System.Drawing.Point(21, 99);
            this.passBox.Name = "passBox";
            this.passBox.Size = new System.Drawing.Size(162, 20);
            this.passBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Heslo:";
            // 
            // targetDirBox
            // 
            this.targetDirBox.AllowDrop = true;
            this.targetDirBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.targetDirBox.Location = new System.Drawing.Point(21, 32);
            this.targetDirBox.Name = "targetDirBox";
            this.targetDirBox.Size = new System.Drawing.Size(162, 20);
            this.targetDirBox.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbDeleteWhenDone);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.targetDirBox);
            this.groupBox1.Controls.Add(this.status);
            this.groupBox1.Controls.Add(this.passBox);
            this.groupBox1.Controls.Add(this.rarBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 176);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zip";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "C�lov� slo�ka:";
            // 
            // cbDeleteWhenDone
            // 
            this.cbDeleteWhenDone.AutoSize = true;
            this.cbDeleteWhenDone.Location = new System.Drawing.Point(21, 58);
            this.cbDeleteWhenDone.Name = "cbDeleteWhenDone";
            this.cbDeleteWhenDone.Size = new System.Drawing.Size(140, 17);
            this.cbDeleteWhenDone.TabIndex = 6;
            this.cbDeleteWhenDone.Text = "Promazat po dokon�en�";
            this.cbDeleteWhenDone.UseVisualStyleBackColor = true;
            // 
            // ZipResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ZipResult";
            this.Size = new System.Drawing.Size(195, 176);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button rarBtn;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.MaskedTextBox passBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox targetDirBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbDeleteWhenDone;
    }
}
