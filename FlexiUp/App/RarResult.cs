using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using FlexiUp.Scripting;

namespace FlexiUp.App {
    public partial class ZipResult : UserControl {

       
        public ZipResult() {
            InitializeComponent();
        }

        private void rarBtn_Click(object sender, EventArgs e) {
            start(targetDirBox.Text);
        }

        private void start(string _resultPath) {
            if (_resultPath == null) {
                status.Text = "Je t�eba ur�it slo�ku pro v�stup.";
                return;
            }

            status.Text = "Pracuju";

            string rar_path = FlexiUp.Properties.Settings.Default.zip_path;
            string pass = passBox.Text;
            passBox.Text = "";
            Application.DoEvents();
            string outFile = Path.Combine(_resultPath, Path.GetFileName(_resultPath) + "." + FlexiUp.Properties.Settings.Default.zip_ext);
            //string args = String.Format("a -r -m5 -hp{0} {1} {2}", pass, outFile, _resultPath);
            string args = String.Format(FlexiUp.Properties.Settings.Default.zip_args, pass, outFile, _resultPath);
            

            Process rar = Process.Start(rar_path, args);
            rar.WaitForExit();
            status.Text = "Hotovo";
        }

        #region Reading data from the outside
        /// <summary>
        /// Fills the form controls with the information from a BackupInfo.
        /// </summary>
        public void ReadFromBackupInfo(BackupInfo sourceData) {
            targetDirBox.Text = sourceData.TargetDir;
        }

        #endregion
    }
}
