using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using FlexiUp.Selectors;
using FlexiUp.Scripting;

namespace FlexiUp.App {
    public partial class ListEditor : Form {
        private SelectedItems itemsToBackup = new SelectedItems();
        private SelectorDict selectorDict;

        public ListEditor() {
            InitializeComponent();

            this.loadSelectors();
            this.rebuildSelectorMenu();

            this.dirSelect.Nodes.Clear();
            this.fillDrives();
        }

        private void loadSelectors() {
            BkListParser parser = new BkListParser(FlexiUpApp.DataDir + "selectors.xml");
            this.selectorDict = parser.ReadInit();
        }

        private void rebuildSelectorMenu() {
            this.selectorMenu.Items.Clear();

            // create the menu items
            foreach (string key in this.selectorDict.Keys) {
                ISelector selector = this.selectorDict[key];
                ToolStripMenuItem item = new ToolStripMenuItem();

                item.Text = key;
                item.Name = key;

                item.Click += new EventHandler(item_Click);
                this.selectorMenu.Items.Add(item);
            }

        }

        private void selectorMenu_Opened(object sender, EventArgs e) {
            TreeNode node = this.dirSelect.SelectedNode;
            if (node == null)
                return;

            string selName = "";
            if (this.itemsToBackup.HasKey(node.FullPath))
                selName = this.itemsToBackup[node.FullPath].SelectorName;

            foreach (ToolStripMenuItem i in this.selectorMenu.Items) {
                i.Checked = (i.Name == selName);
            }


        }

        #region Adding Nodes
        private void fillDrives() {
            foreach (DriveInfo drive in DriveInfo.GetDrives()) {
                this.addNode(this.dirSelect.Nodes, drive.Name.TrimEnd(new char[] {'\\'}), false);
            }
        }

        private TreeNode addNode(TreeNodeCollection treeNodeCollection, string name, bool file) {
            TreeNode node = new TreeNode();
            node.ContextMenuStrip = this.selectorMenu;
            node.Name = name;
            node.Text = name;
            if (!file)
                node.Nodes.Add("dummy");

            node.ImageIndex = file ? 1 : 0;
            node.SelectedImageIndex = node.ImageIndex;

            treeNodeCollection.Add(node);
            return node;
        }

        private void dirSelect_BeforeExpand(object sender, TreeViewCancelEventArgs e) {
            this.expandNode(e.Node);
        }

        private void expandNode(TreeNode treeNode) {
            string path = treeNode.FullPath;
            if (!path.EndsWith("\\"))
                path += "\\";
            treeNode.Nodes.Clear();

            try {
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (DirectoryInfo subdir in dir.GetDirectories()) {
                    TreeNode added = this.addNode(treeNode.Nodes, subdir.Name, false);

                    if (this.itemsToBackup.HasKey(added.FullPath)) {
                        added.Checked = true;
                    }
                }

                // and the files, too
                foreach (FileInfo file in dir.GetFiles()) {
                    TreeNode added = this.addNode(treeNode.Nodes, file.Name, true);

                    if (this.itemsToBackup.HasKey(added.FullPath)) {
                        added.Checked = true;
                    }
                }
            } catch (Exception) {

            }
        }
        #endregion

        #region Manipulating
        private void dirSelect_AfterCheck(object sender, TreeViewEventArgs e) {
            if (e.Node.Checked) {
                // add to the list
                if (!this.itemsToBackup.HasKey(e.Node.FullPath))
                    this.itemsToBackup[e.Node.FullPath] = new BackupItemInfo("all");
                
            } else {
                this.itemsToBackup.Remove(e.Node.FullPath);

            }
        }

        private void item_Click(object sender, EventArgs e) {
            TreeNode node = this.dirSelect.SelectedNode;
            if (node == null)
                return;

            ToolStripMenuItem item = (ToolStripMenuItem)sender;

            this.itemsToBackup[node.FullPath] = new BackupItemInfo(item.Name);
            node.Checked = true;
        }

        private void dirSelect_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {
            this.dirSelect.SelectedNode = e.Node;
        }
        #endregion

        #region Other
        private void saveBtn_Click(object sender, EventArgs e) {
            this.save();
        }

        private void novyToolStripMenuItem_Click(object sender, EventArgs e) {
            this.newList();
        }

        private void newList() {
            this.itemsToBackup = new SelectedItems();
            this.selectorDict = new SelectorDict();
            this.loadSelectors();
        }

        private void otevritToolStripMenuItem_Click(object sender, EventArgs e) {
            this.openList();
        }

        private void openList() {
            this.newList();
            this.openFileDialog.FilterIndex = 1;
            if (this.openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            SelectorDict loaded = this.itemsToBackup.LoadFromXml(this.openFileDialog.FileName);

            this.selectorDict.AddDict(loaded);

            this.rebuildSelectorMenu();
        }

        private void save() {
            DialogResult res =  this.saveFileDialog.ShowDialog(this);
            if (res == DialogResult.OK) {
                this.itemsToBackup.SaveToXml(this.saveFileDialog.FileName, this.selectorDict);
            }
        }
        #endregion

    }
}