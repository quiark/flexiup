using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace FlexiUp.App {
    public partial class SVNCommit : UserControl {
        private Queue<string> dirsToCommit;

        public SVNCommit() {
            InitializeComponent();
        }

        /// <summary>
        /// Opens the dialog to find the list file
        /// </summary>
        private void browseDirList_Click(object sender, EventArgs e) {
            DialogResult res = openFileDialog1.ShowDialog(this);
            if (res == DialogResult.OK) {
                dirList.Text = openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// Initiates the commit action
        /// </summary>
        private void commitBtn_Click(object sender, EventArgs e) {
            Commit(dirList.Text);
        }

        /// <summary>
        /// Commits all files in all directories listed in the file listFileName
        /// </summary>
        public void Commit(string listFileName) {
            dirsToCommit = new Queue<string>();
            using (StreamReader reader = new StreamReader(listFileName)) {
                string line;
                while ((line = reader.ReadLine()) != null) {
                    //commitDir(line.Trim());
                    dirsToCommit.Enqueue(line.Trim());
                }
            }

            nextBtn.Enabled = true;
            next();

        }

        /// <summary>
        /// Commits all files in the directory dirName
        /// </summary>
        private void commitDir(string dirName) {
            appendToLog("\n\nCommiting " + dirName + "\n");

            string svn_path = FlexiUp.Properties.Settings.Default.svn_path;
            string args = String.Format("commit \"{0}\" --message autocommit", dirName);

            ProcessStartInfo svn_start_info = new ProcessStartInfo(svn_path, args);
            svn_start_info.RedirectStandardError = true;
            svn_start_info.RedirectStandardOutput = true;
            svn_start_info.UseShellExecute = false;

            Process svn = Process.Start(svn_start_info);

            svn.ErrorDataReceived += new DataReceivedEventHandler(svn_ErrorDataReceived);
            svn.OutputDataReceived += new DataReceivedEventHandler(svn_OutputDataReceived);
            svn.BeginErrorReadLine();
            svn.BeginOutputReadLine();

            svn.WaitForExit(100*1000);
            if (svn.ExitCode != 0)
                reportFailure();
        }

        void svn_OutputDataReceived(object sender, DataReceivedEventArgs e) {
            appendToLog(e.Data);
        }

        void svn_ErrorDataReceived(object sender, DataReceivedEventArgs e) {
            appendToLog(e.Data);
        }


        private delegate void appendToLogCallback(string text);

        /// <summary>
        /// A thread safe way of using the log control
        /// </summary>
        /// <param name="text"></param>
        private void appendToLog(string text) {
            if (text == null)
                return;

            if (log.InvokeRequired) {
                appendToLogCallback d = new appendToLogCallback(appendToLog);
                this.Invoke(d, new object[] { text });
            } else {
                log.AppendText(text);
                log.AppendText("\n");
            }
        }

        /// <summary>
        /// Informs the user of some failure
        /// </summary>
        private void reportFailure() {
            log.AppendText("Commit FAILED\n");
        }

        private void nextBtn_Click(object sender, EventArgs e) {
            next();
        }

        private void next() {
            commitDir(dirsToCommit.Dequeue());

            if (dirsToCommit.Count == 0)
                nextBtn.Enabled = false;
        }
    }
}
