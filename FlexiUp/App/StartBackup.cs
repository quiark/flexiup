using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using FlexiUp.Scripting;
using System.Linq;
using System.Globalization;

namespace FlexiUp.App {
    public partial class StartBackup : Form, IProgressSink {
        private enum Level
        {
            Message,
            Warning,
            Error
        };

        private class ProgressReport
        {
            public string Message;
            public Level Lvl;
        };

        private BackupTask backup;
        private ListEditor m_ListEditor;
        private bool m_unattended = false;

        private const string SUFFIX_7Z = ".7z";
        private const string SUFFIX_BIG = "-big";

        public StartBackup() {
            InitializeComponent();
            backup = new BackupTask();

            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                LoadScript(args[1]);
                if (tbPassword.Text.Length > 0)
                {
                    m_unattended = true;
                    startBtn_Click(null, null);
                }
            }
        }

        #region GUI
        private void browseSource_Click(object sender, EventArgs e) {
            DialogResult res = this.openFileDialog1.ShowDialog(this);
            if (res != DialogResult.OK)
                return;

            this.source.Text = this.openFileDialog1.FileName;
        }

        private void browseTarget_Click(object sender, EventArgs e) {
            DialogResult res = this.folderBrowserDialog1.ShowDialog(this);
            if (res != DialogResult.OK)
                return;

            this.target.Text = this.folderBrowserDialog1.SelectedPath;
        }

        private void startBtn_Click(object sender, EventArgs e) {
            // init the progress bar
            backupProgressBar.Value = 0;

            backup.Logs.Clear();
            foreach (object obj in this.incLogList.Items) {
                backup.Logs.Add(obj.ToString());
            }
            backup.ProgressSink = this;
            backup.Password = tbPassword.Text;
            tbPassword.Text = "";
            backup.Compress = cbCompress.Checked;
            backupWorker.RunWorkerAsync();
            //backup.Start(source.Text, target.Text);
        }

        private void addLogBtn_Click(object sender, EventArgs e) {
            if (this.openLogDlg.ShowDialog(this) != DialogResult.OK)
                return;

            foreach (string i in this.openLogDlg.FileNames) {
                this.incLogList.Items.Add(i);
            }

            UpdateBigSuffix();
        }

        private void setDefaultDateBtn_Click(object sender, EventArgs e) {
            DateTime now = DateTime.Now;
            string prefix = FlexiUp.Properties.Settings.Default.default_target_prefix;
            string nowStr = String.Format("{0}-{1}-{2}", now.Day, now.Month, now.Year);

            string res = Path.Combine(prefix, nowStr);
            Directory.CreateDirectory(res);
            this.target.Text = res;
        }

        private void bRemoveLog_Click(object sender, EventArgs e) {
            if (incLogList.SelectedIndex != -1) {
                incLogList.Items.RemoveAt(incLogList.SelectedIndex);
                UpdateBigSuffix();
            }
        }
        #endregion

        #region Setting values from the outside

        /// <summary>
        /// Fills the information from the given BackupInfo
        /// </summary>
        public void ReadFromBackupInfo(BackupInfo sourceData) {
            source.Text = sourceData.ListFile;
            incLogList.Items.Clear();
            foreach (string i in sourceData.IncrementalLogFiles) {
                incLogList.Items.Add(i);
            }
            cbCompress.Checked = sourceData.Compress;
            backup.IncrementalLogDb = sourceData.IncrementalLogDb;
            if (sourceData.Password != null) tbPassword.Text = sourceData.Password;

            // 'last big' is the last full backup. We use the month of this backup as part of the
            // target path (using the macro $(lastbigdir)
            Models.BackupDone lastBig = null;
            if (backup.IncrementalLogDb != null) ReadIncLogsFromDb(sourceData.IncrementalLogDb, out lastBig);
            string lastBigFolder;
            if (lastBig != null) {
                lastBigFolder = GetBigFolderName(lastBig.Date);
            } else {
                lastBigFolder = GetBigFolderName(DateTime.Now);
            }
            target.Text = InfoReader.ExpandCustomMacros(sourceData.TargetDir, (x) =>
            {
                if (x == "lastbigdir") return lastBigFolder;
                else return "";
            });

            UpdateBigSuffix();
        }

        private string GetBigFolderName(DateTime date)
        {
            return date.ToString("MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")).ToLowerInvariant();
        }

        private void UpdateBigSuffix()
        {
            UseBigSuffix(incLogList.Items.Count == 0);
        }

        private void UseBigSuffix(bool use)
        {
            int biglen = SUFFIX_BIG.Length;
            int end = target.Text.Length;
            if (target.Text.EndsWith(SUFFIX_7Z)) end -= SUFFIX_7Z.Length;

            if (use)
            {
                target.Text = target.Text.Insert(end, SUFFIX_BIG);
            }
            else
            {
                if (target.Text.Substring(end - biglen, biglen) == SUFFIX_BIG)
                {
                    target.Text = target.Text.Remove(end + 1 - biglen, biglen);
                }
            }
        }

        private void ReadIncLogsFromDb(string incrementalLogDb, out Models.BackupDone lastBig)
        {
            using (var history = HistoryDb.Open(incrementalLogDb) )
            {
                List<Models.BackupDone> list = new List<Models.BackupDone>(history.SelectBackups());
                lastBig = list.Find((x) => x.ModeUsed == Models.BackupDone.Mode.Complete);
                bool makeBig = (lastBig == null) || ((DateTime.Now - lastBig.Date).Days > (30 * 6));
                if (makeBig) return;

                foreach (var i in list)
                {
                    incLogList.Items.Add(i.LogFile);
                    if (i.ModeUsed == Models.BackupDone.Mode.Complete) break; // no need to include previous logs
                }
            }
        }

        #endregion

        #region Worker thread
        private void backupWorker_DoWork(object sender, DoWorkEventArgs e) {
            try {
                backup.Start(this.source.Text, this.target.Text);
            } catch (Exception exc) {
                ((IProgressSink)this).Error(exc.ToString());
            }
        }
        #endregion

        private void backupWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            backupProgressBar.Value = backupProgressBar.Maximum;

            // and no error
            if (m_unattended)
            {
                this.Close();
            }
        }

        private void backupWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            ProgressReport report = e.UserState as ProgressReport;
            if (report == null) {
                backupProgressBar.Value = e.ProgressPercentage;
            } else {
                lbStatus.Items.Add(String.Format("[{0}] {1}" , DateTime.Now.ToString(), report.Message));
                if (report.Lvl != Level.Message) m_unattended = false;
            }
        }

        #region IProgressSink Members

        float IProgressSink.Progress {
            set {
                backupWorker.ReportProgress((int)(value * 100));
            }
        }

        void IProgressSink.Error(string Message) {
            backupWorker.ReportProgress(0, new ProgressReport {
                Message = Message,
                Lvl = Level.Error
            });
        }

        void IProgressSink.Warning(string Message)
        {
            backupWorker.ReportProgress(0, new ProgressReport
            {
                Message = Message,
                Lvl = Level.Warning
            });
        }

        void IProgressSink.Message(string Message) {
            backupWorker.ReportProgress(0, new ProgressReport {
                Message = Message,
                Lvl = Level.Message
            });
        }

        #endregion

        private void bOpenScript_Click(object sender, EventArgs e)
        {
            DialogResult res = dOpenScript.ShowDialog(this);
            if (res != DialogResult.OK)
                return;

            LoadScript(dOpenScript.FileName);
        }

        private void LoadScript(string filename)
        {
            BackupInfo info;
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                InfoReader reader = new InfoReader();
                info = reader.ReadStream(fs);
            }

            if (info != null)
            {
                this.ReadFromBackupInfo(info);
            }
        }

        private void cbCompress_CheckedChanged(object sender, EventArgs e) {
            const string sz = SUFFIX_7Z;
            if (cbCompress.Checked) {
                if (!target.Text.EndsWith(sz))
                    target.Text += sz;

            } else {
                if (target.Text.EndsWith(sz))
                    target.Text = target.Text.Substring(0, target.Text.Length - sz.Length);
            }
        }

        private void bOpenEditor_Click(object sender, EventArgs e) {
            if ((m_ListEditor == null) || (m_ListEditor.IsDisposed)) {
                m_ListEditor = new ListEditor();
            }
            m_ListEditor.Show();

        }


    }
}
