namespace FlexiUp.App {
    partial class ListEditor {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListEditor));
            this.dirSelect = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.selectorMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otevritToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ulozitJakoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.rarResult = new FlexiUp.App.ZipResult();
            this.svnCommit1 = new FlexiUp.App.SVNCommit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dirSelect
            // 
            this.dirSelect.CheckBoxes = true;
            this.dirSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dirSelect.FullRowSelect = true;
            this.dirSelect.HotTracking = true;
            this.dirSelect.ImageIndex = 0;
            this.dirSelect.ImageList = this.imageList;
            this.dirSelect.Location = new System.Drawing.Point(0, 0);
            this.dirSelect.Name = "dirSelect";
            this.dirSelect.SelectedImageIndex = 0;
            this.dirSelect.Size = new System.Drawing.Size(315, 313);
            this.dirSelect.TabIndex = 0;
            this.dirSelect.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.dirSelect_AfterCheck);
            this.dirSelect.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.dirSelect_BeforeExpand);
            this.dirSelect.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.dirSelect_NodeMouseClick);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "folder_orange.png");
            this.imageList.Images.SetKeyName(1, "unknown.png");
            // 
            // selectorMenu
            // 
            this.selectorMenu.Name = "selectorMenu";
            this.selectorMenu.Size = new System.Drawing.Size(61, 4);
            this.selectorMenu.Opened += new System.EventHandler(this.selectorMenu_Opened);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dirSelect);
            this.splitContainer1.Size = new System.Drawing.Size(683, 313);
            this.splitContainer1.SplitterDistance = 315;
            this.splitContainer1.TabIndex = 1;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Backup list (*.bklist)|*.bklist";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(711, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novyToolStripMenuItem,
            this.otevritToolStripMenuItem,
            this.ulozitJakoToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // novyToolStripMenuItem
            // 
            this.novyToolStripMenuItem.Name = "novyToolStripMenuItem";
            this.novyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.novyToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.novyToolStripMenuItem.Text = "Nov�";
            this.novyToolStripMenuItem.Click += new System.EventHandler(this.novyToolStripMenuItem_Click);
            // 
            // otevritToolStripMenuItem
            // 
            this.otevritToolStripMenuItem.Name = "otevritToolStripMenuItem";
            this.otevritToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.otevritToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.otevritToolStripMenuItem.Text = "Otev��t...";
            this.otevritToolStripMenuItem.Click += new System.EventHandler(this.otevritToolStripMenuItem_Click);
            // 
            // ulozitJakoToolStripMenuItem
            // 
            this.ulozitJakoToolStripMenuItem.Name = "ulozitJakoToolStripMenuItem";
            this.ulozitJakoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.ulozitJakoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.ulozitJakoToolStripMenuItem.Text = "Ulo�it jako...";
            this.ulozitJakoToolStripMenuItem.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Seznam soubor� (*.bklist)|*.bklist|Z�lohovac� skript (*.bkscript)|*.bkscript|V�ec" +
                "hny soubory|*.*";
            // 
            // rarResult
            // 
            this.rarResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rarResult.Location = new System.Drawing.Point(415, 346);
            this.rarResult.Name = "rarResult";
            this.rarResult.Size = new System.Drawing.Size(240, 190);
            this.rarResult.TabIndex = 4;
            // 
            // svnCommit1
            // 
            this.svnCommit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.svnCommit1.Location = new System.Drawing.Point(0, 346);
            this.svnCommit1.Name = "svnCommit1";
            this.svnCommit1.Size = new System.Drawing.Size(409, 199);
            this.svnCommit1.TabIndex = 3;
            // 
            // ListEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 548);
            this.Controls.Add(this.rarResult);
            this.Controls.Add(this.svnCommit1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ListEditor";
            this.Text = "ListEditor";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView dirSelect;
        private System.Windows.Forms.ContextMenuStrip selectorMenu;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otevritToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ulozitJakoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private SVNCommit svnCommit1;
        private ZipResult rarResult;
    }
}