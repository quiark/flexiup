using FlexiUp.Scripting;
namespace FlexiUp.App {
    partial class StartBackup {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.startBtn = new System.Windows.Forms.Button();
            this.source = new System.Windows.Forms.TextBox();
            this.target = new System.Windows.Forms.TextBox();
            this.browseSource = new System.Windows.Forms.Button();
            this.browseTarget = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.openLogDlg = new System.Windows.Forms.OpenFileDialog();
            this.incPanel = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.bRemoveLog = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.addLogBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.incLogList = new System.Windows.Forms.ListBox();
            this.setDefaultDateBtn = new System.Windows.Forms.Button();
            this.backupProgressBar = new System.Windows.Forms.ProgressBar();
            this.backupWorker = new System.ComponentModel.BackgroundWorker();
            this.inOutPanel = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.cbCompress = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.topSplit = new System.Windows.Forms.SplitContainer();
            this.bOpenScript = new System.Windows.Forms.Button();
            this.bOpenEditor = new System.Windows.Forms.Button();
            this.lbStatus = new System.Windows.Forms.ListBox();
            this.dOpenScript = new System.Windows.Forms.OpenFileDialog();
            this.incPanel.SuspendLayout();
            this.inOutPanel.SuspendLayout();
            this.topSplit.Panel1.SuspendLayout();
            this.topSplit.Panel2.SuspendLayout();
            this.topSplit.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Backup list (*.bklist)|*.bklist";
            // 
            // startBtn
            // 
            this.startBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.startBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.startBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.startBtn.Location = new System.Drawing.Point(19, 168);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(345, 23);
            this.startBtn.TabIndex = 0;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // source
            // 
            this.source.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.source.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(204)))), ((int)(((byte)(128)))));
            this.source.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.source.Location = new System.Drawing.Point(19, 28);
            this.source.Name = "source";
            this.source.Size = new System.Drawing.Size(295, 22);
            this.source.TabIndex = 1;
            // 
            // target
            // 
            this.target.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.target.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(204)))), ((int)(((byte)(128)))));
            this.target.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.target.Location = new System.Drawing.Point(19, 74);
            this.target.Name = "target";
            this.target.Size = new System.Drawing.Size(264, 22);
            this.target.TabIndex = 2;
            // 
            // browseSource
            // 
            this.browseSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseSource.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.browseSource.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.browseSource.Location = new System.Drawing.Point(320, 25);
            this.browseSource.Name = "browseSource";
            this.browseSource.Size = new System.Drawing.Size(44, 23);
            this.browseSource.TabIndex = 3;
            this.browseSource.Text = "...";
            this.browseSource.UseVisualStyleBackColor = true;
            this.browseSource.Click += new System.EventHandler(this.browseSource_Click);
            // 
            // browseTarget
            // 
            this.browseTarget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseTarget.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.browseTarget.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.browseTarget.Location = new System.Drawing.Point(336, 71);
            this.browseTarget.Name = "browseTarget";
            this.browseTarget.Size = new System.Drawing.Size(28, 23);
            this.browseTarget.TabIndex = 4;
            this.browseTarget.Text = "...";
            this.browseTarget.UseVisualStyleBackColor = true;
            this.browseTarget.Click += new System.EventHandler(this.browseTarget_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(139)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(16, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Zdrojov� soubor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(139)))), ((int)(((byte)(8)))));
            this.label2.Location = new System.Drawing.Point(16, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "C�lov� adres��:";
            // 
            // openLogDlg
            // 
            this.openLogDlg.FileName = "openFileDialog2";
            this.openLogDlg.Filter = "Z�znamy (*.log)|*.log";
            this.openLogDlg.Multiselect = true;
            // 
            // incPanel
            // 
            this.incPanel.Controls.Add(this.label7);
            this.incPanel.Controls.Add(this.bRemoveLog);
            this.incPanel.Controls.Add(this.label4);
            this.incPanel.Controls.Add(this.addLogBtn);
            this.incPanel.Controls.Add(this.label3);
            this.incPanel.Controls.Add(this.incLogList);
            this.incPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incPanel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.incPanel.Location = new System.Drawing.Point(0, 0);
            this.incPanel.Name = "incPanel";
            this.incPanel.Size = new System.Drawing.Size(361, 243);
            this.incPanel.TabIndex = 7;
            this.incPanel.Text = "Navy�ovac� z�loha";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(227)))), ((int)(((byte)(156)))));
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 243);
            this.label7.TabIndex = 10;
            // 
            // bRemoveLog
            // 
            this.bRemoveLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bRemoveLog.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bRemoveLog.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bRemoveLog.Location = new System.Drawing.Point(208, 28);
            this.bRemoveLog.Name = "bRemoveLog";
            this.bRemoveLog.Size = new System.Drawing.Size(147, 23);
            this.bRemoveLog.TabIndex = 5;
            this.bRemoveLog.Text = "Odmazat z�znam";
            this.bRemoveLog.UseVisualStyleBackColor = true;
            this.bRemoveLog.Click += new System.EventHandler(this.bRemoveLog_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(190, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Je pot�eba vlo�it v�echny logy. ";
            // 
            // addLogBtn
            // 
            this.addLogBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.addLogBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addLogBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.addLogBtn.Location = new System.Drawing.Point(16, 28);
            this.addLogBtn.Name = "addLogBtn";
            this.addLogBtn.Size = new System.Drawing.Size(186, 23);
            this.addLogBtn.TabIndex = 3;
            this.addLogBtn.Text = "P�idat z�znam";
            this.addLogBtn.UseVisualStyleBackColor = true;
            this.addLogBtn.Click += new System.EventHandler(this.addLogBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(139)))), ((int)(((byte)(8)))));
            this.label3.Location = new System.Drawing.Point(13, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Z�klad z�lohy:";
            // 
            // incLogList
            // 
            this.incLogList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.incLogList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(204)))), ((int)(((byte)(128)))));
            this.incLogList.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.incLogList.FormattingEnabled = true;
            this.incLogList.HorizontalScrollbar = true;
            this.incLogList.IntegralHeight = false;
            this.incLogList.Location = new System.Drawing.Point(16, 57);
            this.incLogList.Name = "incLogList";
            this.incLogList.Size = new System.Drawing.Size(339, 183);
            this.incLogList.TabIndex = 1;
            // 
            // setDefaultDateBtn
            // 
            this.setDefaultDateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.setDefaultDateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.setDefaultDateBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.setDefaultDateBtn.Location = new System.Drawing.Point(289, 72);
            this.setDefaultDateBtn.Name = "setDefaultDateBtn";
            this.setDefaultDateBtn.Size = new System.Drawing.Size(41, 23);
            this.setDefaultDateBtn.TabIndex = 8;
            this.setDefaultDateBtn.Text = "dnes";
            this.setDefaultDateBtn.UseVisualStyleBackColor = true;
            this.setDefaultDateBtn.Click += new System.EventHandler(this.setDefaultDateBtn_Click);
            // 
            // backupProgressBar
            // 
            this.backupProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.backupProgressBar.Location = new System.Drawing.Point(19, 197);
            this.backupProgressBar.Name = "backupProgressBar";
            this.backupProgressBar.Size = new System.Drawing.Size(343, 25);
            this.backupProgressBar.TabIndex = 0;
            // 
            // backupWorker
            // 
            this.backupWorker.WorkerReportsProgress = true;
            this.backupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backupWorker_DoWork);
            this.backupWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backupWorker_ProgressChanged);
            this.backupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backupWorker_RunWorkerCompleted);
            // 
            // inOutPanel
            // 
            this.inOutPanel.Controls.Add(this.label8);
            this.inOutPanel.Controls.Add(this.tbPassword);
            this.inOutPanel.Controls.Add(this.cbCompress);
            this.inOutPanel.Controls.Add(this.label6);
            this.inOutPanel.Controls.Add(this.target);
            this.inOutPanel.Controls.Add(this.backupProgressBar);
            this.inOutPanel.Controls.Add(this.startBtn);
            this.inOutPanel.Controls.Add(this.source);
            this.inOutPanel.Controls.Add(this.setDefaultDateBtn);
            this.inOutPanel.Controls.Add(this.browseSource);
            this.inOutPanel.Controls.Add(this.browseTarget);
            this.inOutPanel.Controls.Add(this.label2);
            this.inOutPanel.Controls.Add(this.label1);
            this.inOutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inOutPanel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.inOutPanel.Location = new System.Drawing.Point(0, 0);
            this.inOutPanel.Name = "inOutPanel";
            this.inOutPanel.Size = new System.Drawing.Size(367, 243);
            this.inOutPanel.TabIndex = 10;
            this.inOutPanel.Text = "groupBox3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(139)))), ((int)(((byte)(8)))));
            this.label8.Location = new System.Drawing.Point(19, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 15);
            this.label8.TabIndex = 12;
            this.label8.Text = "Heslo:";
            // 
            // tbPassword
            // 
            this.tbPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(204)))), ((int)(((byte)(128)))));
            this.tbPassword.Location = new System.Drawing.Point(19, 140);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(348, 22);
            this.tbPassword.TabIndex = 11;
            // 
            // cbCompress
            // 
            this.cbCompress.AutoSize = true;
            this.cbCompress.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.cbCompress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(139)))), ((int)(((byte)(8)))));
            this.cbCompress.Location = new System.Drawing.Point(19, 102);
            this.cbCompress.Name = "cbCompress";
            this.cbCompress.Size = new System.Drawing.Size(61, 19);
            this.cbCompress.TabIndex = 10;
            this.cbCompress.Text = "Stla�it";
            this.cbCompress.UseVisualStyleBackColor = true;
            this.cbCompress.CheckedChanged += new System.EventHandler(this.cbCompress_CheckedChanged);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(227)))), ((int)(((byte)(156)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 243);
            this.label6.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(170)))), ((int)(((byte)(45)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(250)))), ((int)(((byte)(222)))));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(732, 34);
            this.label5.TabIndex = 11;
            this.label5.Text = "FlexiUp";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // topSplit
            // 
            this.topSplit.Dock = System.Windows.Forms.DockStyle.Top;
            this.topSplit.Location = new System.Drawing.Point(0, 34);
            this.topSplit.Name = "topSplit";
            // 
            // topSplit.Panel1
            // 
            this.topSplit.Panel1.Controls.Add(this.inOutPanel);
            // 
            // topSplit.Panel2
            // 
            this.topSplit.Panel2.Controls.Add(this.incPanel);
            this.topSplit.Size = new System.Drawing.Size(732, 243);
            this.topSplit.SplitterDistance = 367;
            this.topSplit.TabIndex = 12;
            // 
            // bOpenScript
            // 
            this.bOpenScript.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bOpenScript.Location = new System.Drawing.Point(3, 5);
            this.bOpenScript.Name = "bOpenScript";
            this.bOpenScript.Size = new System.Drawing.Size(142, 23);
            this.bOpenScript.TabIndex = 15;
            this.bOpenScript.Text = "Otev��t skript...";
            this.bOpenScript.UseVisualStyleBackColor = true;
            this.bOpenScript.Click += new System.EventHandler(this.bOpenScript_Click);
            // 
            // bOpenEditor
            // 
            this.bOpenEditor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bOpenEditor.Location = new System.Drawing.Point(151, 5);
            this.bOpenEditor.Name = "bOpenEditor";
            this.bOpenEditor.Size = new System.Drawing.Size(142, 23);
            this.bOpenEditor.TabIndex = 16;
            this.bOpenEditor.Text = "Editor popis�...";
            this.bOpenEditor.UseVisualStyleBackColor = true;
            this.bOpenEditor.Click += new System.EventHandler(this.bOpenEditor_Click);
            // 
            // lbStatus
            // 
            this.lbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(204)))), ((int)(((byte)(128)))));
            this.lbStatus.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lbStatus.FormattingEnabled = true;
            this.lbStatus.ItemHeight = 15;
            this.lbStatus.Location = new System.Drawing.Point(0, 283);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(732, 214);
            this.lbStatus.TabIndex = 17;
            // 
            // dOpenScript
            // 
            this.dOpenScript.FileName = "openFileDialog2";
            // 
            // StartBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(250)))), ((int)(((byte)(222)))));
            this.ClientSize = new System.Drawing.Size(732, 503);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.bOpenEditor);
            this.Controls.Add(this.bOpenScript);
            this.Controls.Add(this.topSplit);
            this.Controls.Add(this.label5);
            this.Name = "StartBackup";
            this.Text = "FlexiUp";
            this.incPanel.ResumeLayout(false);
            this.incPanel.PerformLayout();
            this.inOutPanel.ResumeLayout(false);
            this.inOutPanel.PerformLayout();
            this.topSplit.Panel1.ResumeLayout(false);
            this.topSplit.Panel2.ResumeLayout(false);
            this.topSplit.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.TextBox source;
        private System.Windows.Forms.TextBox target;
        private System.Windows.Forms.Button browseSource;
        private System.Windows.Forms.Button browseTarget;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openLogDlg;
        private System.Windows.Forms.Panel incPanel;
        private System.Windows.Forms.ListBox incLogList;
        private System.Windows.Forms.Button addLogBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button setDefaultDateBtn;
        private System.Windows.Forms.ProgressBar backupProgressBar;
        private System.Windows.Forms.Label label4;
        private System.ComponentModel.BackgroundWorker backupWorker;
        private System.Windows.Forms.Button bRemoveLog;
        private System.Windows.Forms.Panel inOutPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.SplitContainer topSplit;
        private System.Windows.Forms.Button bOpenScript;
        private System.Windows.Forms.Button bOpenEditor;
        private System.Windows.Forms.ListBox lbStatus;
        private System.Windows.Forms.OpenFileDialog dOpenScript;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.CheckBox cbCompress;
    }
}
