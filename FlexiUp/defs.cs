using System;
using System.Collections.Generic;
using FlexiUp.Selectors;

using NUnit.Framework;

/// <summary>
/// Assigns selectors to textual names, as read from the bklist file.
/// </summary>
public class SelectorDict : Dictionary<string, ISelector> {
    public void AddDict(SelectorDict rhs) {
        ISelector v;
        foreach (string key in rhs.Keys) {
            if (this.TryGetValue(key, out v)) {
                this[key] = rhs[key];
            } else {
                this.Add(key, rhs[key]);
            }
        }
    }
}

public class MyAssert {
    public static void Contains<T>(T item, ICollection<T> list) {
        bool contains = list.Contains(item);
        if (contains) {
            Assert.IsTrue(true);
        } else {
            Assert.Fail("Item {0} expected in {1}", item.ToString(), list.ToString());
        }
    }

    public static void NotContains<T>(T item, ICollection<T> list) {
        bool contains = list.Contains(item);

        if (!contains) {
            Assert.IsTrue(true);
        } else {
            Assert.Fail("Item {0} should not be present in {1}", item.ToString(), list.ToString());
        }

    }

    public static void ContainsPath(string item, ICollection<string> list) {
        Contains<string>(item.ToLower(), list);
    }

    public static void NotContainsPath(string item, ICollection<string> list) {
        NotContains<string>(item.ToLower(), list);
    }
}