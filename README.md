# Flexible backUp system #

So, umm.... it supports:

* specifying folder types (VS project, TeX project, ...) and exclude boring files (.log, .obj) in each of them
* incremental backups (file based)
* compression and encryption using 7zip
* keeping track of previous backups so incremental logs can be managed automatically
* Passing script name on commandline (that would allow unattended backup except for password)
* Do a full backup every half-year
* Save password in the script ... and use that password for backups only


## To Do ##

* Output to folder based on last full backup (april 2016)
* Run external command after backup finishes
* How to manage old backup files (deleting)?
	* How to know which files are incremental?

