﻿module Expected

type expected<'t, 'e> =
    | Ok of 't
    | Error of 'e

let isOk = function
    | Ok _ -> true
    | Error _ -> false

let isError v = isOk v |> not