﻿(*
First run:
    DiffBird update

    -> save to signature_dir
    -> copy full file to full_dir
    . backup full and also signature

Next run
    DiffBird update

    .  clear delta and full folders
    -> create deltas to delta_dir
    -> save (new file) signatures to signature_dir
    -> copy full file to full_dir
    .  compress & encrypt
    .  upload

Restore
    DiffBird restore

    -> take full data
    -> take last delta
    -> write orig file


* signature dir = static
* full_dir = static
* delta_dir = with date


TODO:
    + examine all error cases
*)
open System
open System.IO
open System.Text
open System.Diagnostics
open System.Configuration

let cfg (k:string) = ConfigurationManager.AppSettings.Get(k)

let RDIFF_BIN = cfg "rdiff_bin"
let DATA_DIR = cfg "data_dir"
let SIGNATURE_DIR = cfg "signature_dir"
let DELTA_DIR = cfg "delta_dir"
let RESTORE_DIR = cfg "restore_dir"
let BACKUP_FULL_DIR = cfg "backup_full_dir"

let SIZE_MIN = 5*1000*1000 |> int64    // unused


let run_rdiff (args: string array) =
    //printf "rdiff %A\n" args
    let pinfo = new ProcessStartInfo(RDIFF_BIN)
    pinfo.Arguments <- String.Join(" ", Array.map (fun x -> "\"" + x + "\"") args)
    pinfo.RedirectStandardError <- true
    pinfo.UseShellExecute <- false

    let proc = new Process()
    proc.StartInfo <- pinfo
    proc.Start() |> ignore
    let err = proc.StandardError.ReadToEnd()

    proc.WaitForExit()
    if (proc.ExitCode = 0) && (err.Length = 0)
    then Expected.Ok ()
    else Expected.Error err

let run_rdiff_catch (args: string array) =
    try run_rdiff args
    with e -> Expected.Error (e.ToString())
 
 // --- path utilities
let get_relpath_to (to_:string) (fname:string) =
    assert fname.StartsWith(to_)
    let rplen = 
        if to_.EndsWith("\\")
        then to_.Length
        else to_.Length + 1
    fname.Substring(rplen)

let get_relpath (fname:string) =
    get_relpath_to DATA_DIR fname

let create_dir_for fname =
    let dir = Path.GetDirectoryName(fname)
    Directory.CreateDirectory(dir) |> ignore

let clean_file fname =
    if File.Exists(fname) then File.Delete(fname)

// --- rdiff main functions
// returns true <==> valid sig file created
let create_sig (file:FileInfo) =
    let fname = file.FullName
    let relpath = get_relpath fname
    let sig_name = Path.Combine(SIGNATURE_DIR, relpath) 
    create_dir_for sig_name

    let ok = run_rdiff_catch [| "signature"; fname; sig_name |]
    if Expected.isError ok then clean_file sig_name
    ok

type DeltaResult = DeltaOk | NoSignature | DeltaFail of string

// returns DeltaOk <==> delta file exists and is valid
let create_delta (file:FileInfo) = 
    let relpath = get_relpath file.FullName
    let sig_file = Path.Combine(SIGNATURE_DIR, relpath)
    let delta_file = Path.Combine(DELTA_DIR, relpath)
    if File.Exists(sig_file) then 
        create_dir_for delta_file
        match run_rdiff_catch [| "delta"; sig_file; file.FullName; delta_file |] with
         | Expected.Ok _ -> DeltaOk
         | Expected.Error err ->
            clean_file delta_file
            DeltaFail err
    else NoSignature

let copy_orig (file:FileInfo) =
    try
        let relpath = get_relpath file.FullName
        let dest_path = Path.Combine(BACKUP_FULL_DIR, relpath)
        create_dir_for dest_path
        File.Copy(file.FullName, dest_path)
        Expected.Ok ()
    with e ->
        Expected.Error (e.ToString())

let update_file (file:FileInfo) = 
    let delta_res = create_delta file
    if delta_res = DeltaOk then Expected.Ok ()
    else match (create_sig file) with
         | Expected.Ok _ -> copy_orig file
         | Expected.Error er -> Expected.Error er


let restore_file (base_file:FileInfo) =
    let relpath = get_relpath_to BACKUP_FULL_DIR base_file.FullName
    let out_file = Path.Combine(RESTORE_DIR, relpath)
    let delta_file = Path.Combine(DELTA_DIR, relpath)
    create_dir_for out_file
    run_rdiff [| "patch"; base_file.FullName; delta_file; out_file |]


// --- post process functions
let handle_failed op (lst:(FileInfo * Expected.expected<'a, string>) seq) =
    Directory.CreateDirectory(DELTA_DIR) |> ignore
    let logf = new StreamWriter(Path.Combine(DELTA_DIR, "log.txt"), false, Encoding.UTF8)
    fprintf logf "Failed files in %A:" op
    Seq.iter (fun (f:FileInfo, e) -> 
        fprintf logf "\t%A\n%A\n" f.FullName e
    ) lst
    logf.Close()

let zip_result f =
    fun x -> (x, f x)
let result_iserr (t, e) =
    Expected.isError e

// Restores all files from original full backup and deltas.
// Uses these globals:
//  * BACKUP_FULL_DIR for reading original full file versions
//  * DELTA_DIR for the deltas to apply
//  * RESTORE_DIR for the final output
// returns number of failed files
let run_restore () =
    Directory.CreateDirectory(RESTORE_DIR) |> ignore
    let src = new DirectoryInfo(BACKUP_FULL_DIR)
    let failed = 
        src.EnumerateFiles("*", SearchOption.AllDirectories)
        |> Seq.map (zip_result restore_file)
        |> Seq.filter result_iserr
        |> List.ofSeq
    handle_failed "restore" failed
    List.length failed

let prepare_backup () =
    if Directory.Exists(DELTA_DIR) then
        Directory.Delete(DELTA_DIR, true)
    if Directory.Exists(BACKUP_FULL_DIR) then
        Directory.Delete(BACKUP_FULL_DIR, true)

let run_update () =
    prepare_backup ()
    let dir = new DirectoryInfo(DATA_DIR)
    let failed =
        dir.EnumerateFiles("*", SearchOption.AllDirectories)
        |> Seq.map (zip_result update_file)
        |> Seq.filter result_iserr
        |> List.ofSeq
    handle_failed "update" failed
    List.length failed



let self_test () =
    let file_cmp one two =
        let info1 = new FileInfo(one)
        let info2 = new FileInfo(two)
        if info1.Length <> info2.Length then false
        else
            let data1 = File.ReadAllBytes(one)
            let data2 = File.ReadAllBytes(two)
            data1 = data2

    let seq_all pr seq = Seq.exists (pr >> not) seq |> not
    let all_files = ["testA.dat"; "testB.dat"; "dir\\testC.dat"]
    let safe_delete (d:string) =
        if Directory.Exists(d) then
            assert (d.Contains("Temp"))
            Directory.Delete(d, true)

    // initial cleanup (must be deleting testing folder)
    safe_delete BACKUP_FULL_DIR
    safe_delete RESTORE_DIR
    safe_delete DELTA_DIR
    safe_delete SIGNATURE_DIR
    let orig_v1 = BACKUP_FULL_DIR + "1"
    let orig_v2 = Path.Combine(Path.GetDirectoryName(DATA_DIR), "orig_v2")
    safe_delete orig_v1
    safe_delete orig_v2

    // assuming some testing data under configured folders
    // first run - copy full and create sig
    assert (0 = run_update ())
    assert (file_cmp (Path.Combine(BACKUP_FULL_DIR, "testA.dat")) (Path.Combine(DATA_DIR, "testA.dat")))
    assert (file_cmp (Path.Combine(BACKUP_FULL_DIR, "testB.dat")) (Path.Combine(DATA_DIR, "testB.dat")))
    assert (file_cmp (Path.Combine(BACKUP_FULL_DIR, "dir", "testC.dat")) (Path.Combine(DATA_DIR, "dir", "testC.dat")))

    assert (seq_all (fun f -> File.Exists(Path.Combine(SIGNATURE_DIR, f))) all_files)

    // save backup
    Directory.Move(BACKUP_FULL_DIR, orig_v1)

    // make changes
    using (File.AppendText(Path.Combine(DATA_DIR, "testA.dat"))) (fun f -> f.Write("xxxxxxxxxxxxxxxxxxxxx"))
    let fB = new FileStream(Path.Combine(DATA_DIR, "dir", "testC.dat"), FileMode.Open)
    fB.SetLength(1000L * 1000L)
    fB.Write([|0uy; 1uy; 2uy; 3uy; 4uy; 5uy; 6uy; 7uy; 8uy; 9uy|], 0, 10)
    fB.Close()

    // create delta
    assert (0 = run_update ())

    // save version 2 of files
    Directory.CreateDirectory(orig_v2) |> ignore
    Seq.iter (fun f -> 
        let tgt = Path.Combine(orig_v2, f)
        create_dir_for tgt
        File.Copy(Path.Combine(DATA_DIR, f), tgt)) all_files

    // restore
    Directory.Move(orig_v1, BACKUP_FULL_DIR)
    assert (0 = run_restore ())

    // compare files
    assert (seq_all (fun f -> file_cmp (Path.Combine(RESTORE_DIR, f)) (Path.Combine(orig_v2, f))) all_files)


[<EntryPoint>]
let main (argv:string array) = 
    let op = if argv.Length < 1
             then "help"
             else argv.[0]
    match op with
    | "update" -> 
        run_update ()
    | "restore" -> 
        run_restore ()
    | "test" ->
        self_test ()
        0
    | _ ->
        printf "Usage: DiffBird update | restore\n"
        0